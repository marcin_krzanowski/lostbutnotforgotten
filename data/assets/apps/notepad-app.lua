local composer = require( "composer" )
local json = require "json"
local widget = require("widget")
local audio = require("audio");

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------

local scripts, commands, custom, once

local graphElements, separators, gnotes

local closeScene = function(event)
	local options =
		{
			effect = "fade",
			time = 100,
		}
	if once == nil then
		--commands.answer("")
		once = 1;
	end
	--commands.incent();
	composer.gotoScene( "main-scene", options )
	return true end


-- "scene:create()"
function scene:create( event )
	separators = {}
	gnotes = {}
	local sceneGroup = self.view

	scripts = event.params.scripts
	commands = event.params.scripts.coms
	custom = event.params.custom

	graphElements = {}

	-- Initialize the scene here.
	-- Example: add display objects to "sceneGroup", add touch listeners, etc.


	graphElements.bcg = display.newRect(sceneGroup, display.contentWidth/2, display.contentHeight/2, 3000, 3000)
	graphElements.bcg:setFillColor(unpack(commands.bcgColor))

	--graphElements.bcg:addEventListener("tap", closeScene)

	graphElements.txt_welcome  = display.newText({
		text = "DATNote by C&B LTD.",
		x = 0,
		y = 0,
		font = composer.font,
		fontSize = commands.fontSize,
		align = "left",
		width = 760,
		height = 0
	})
	sceneGroup:insert(graphElements.txt_welcome)
	graphElements.txt_welcome:setFillColor(unpack(commands.defColor))
	graphElements.txt_welcome.anchorX=0
	graphElements.txt_welcome.anchorY=0
	graphElements.txt_welcome.x=10
	graphElements.txt_welcome.y=10


	graphElements.mainView =  widget.newScrollView
		{
			top = 50 + graphElements.txt_welcome.height,
			left = 20,
			width = 760,
			height = 1150 - 50 - graphElements.txt_welcome.height,
			hideBackground = false,
			backgroundColor = commands.bcgColor,
			horizontalScrollDisabled = true,
			verticalScrollDisabled = false,
			isBounceEnabled = false
	}
	sceneGroup:insert(graphElements.mainView)

	graphElements.menuButton  = display.newText({
		text = "Close",
		x = 0,
		y = 0,
		font = composer.font,
		fontSize = commands.fontSize,
		align = "left",
		width = 430,
		height = 90
	})
	sceneGroup:insert(graphElements.menuButton)
	graphElements.menuButton:setFillColor(unpack(commands.defColor))
	graphElements.menuButton.anchorX=0
	graphElements.menuButton.anchorY=0
	graphElements.menuButton.x=100
	graphElements.menuButton.y=graphElements.mainView.height + 110 + graphElements.txt_welcome.height
	graphElements.menuButton:addEventListener("tap", closeScene)

end

local function drawNotes(sceneGroup)
	if table.getn(scripts.coms.notes) < 1 then

		local obj = display.newText({
			text = "Sorry, no notes yet :<\nStart writing your own!\nType \"note help\" for more info!",
			x = 0,
			y = 0,
			font = composer.font,
			fontSize = commands.fontSize,
			align = "left",
			width = 740,
			height = 0
		})
		obj:setFillColor(unpack(commands.defColor))
		obj.anchorX=0
		obj.anchorY=0
		obj.x=10
		obj.y=0

		graphElements.mainView:insert(obj);
		gnotes[1] = obj;

	else

		local len = 0;
		local iter = 1;

		for k,v in ipairs(commands.notes) do
			local obj = display.newText({
				text = v[1] .. "\n" .. v[2],
				x = 0,
				y = 0,
				font = composer.font,
				fontSize = commands.fontSize,
				align = "left",
				width = 740,
				height = 0
			})
			obj:setFillColor(unpack(commands.defColor))
			obj.anchorX=0
			obj.anchorY=0
			obj.x=10
			obj.y=len
			obj.iter = iter

			len = len + obj.height + 41--+ obj.height/2;

			obj:addEventListener( "tap", function(event)
				--listener to remove note
				local popupGraph = {}
				popupGraph.bcg = display.newRect(sceneGroup, display.contentWidth/2, display.contentHeight/2, 3000, 3000)
				popupGraph.bcg:setFillColor(unpack(commands.bcgColor)) --hide notes

				local text = v[2]
				if string.len(text) > 50 then
					text = string.sub(text, 1, 50);

				end

				popupGraph.txt_welcome  = display.newText({
					text = "Do you want to remove this message?\n\n" .. v[1] .. "\n" .. text .. "...",
					x = 0,
					y = 0,
					font = composer.font,
					fontSize = commands.fontSize,
					align = "left",
					width = 760,
					height = 0
				})
				sceneGroup:insert(popupGraph.txt_welcome)
				popupGraph.txt_welcome:setFillColor(unpack(commands.defColor))
				popupGraph.txt_welcome.anchorX=0
				popupGraph.txt_welcome.anchorY=0
				popupGraph.txt_welcome.x=10
				popupGraph.txt_welcome.y=150

				popupGraph.bButton  = display.newText({
					text = "Back",
					x = 0,
					y = 0,
					font = composer.font,
					fontSize = commands.fontSize,
					align = "left",
					width = 430,
					height = 90
				})
				sceneGroup:insert(popupGraph.bButton)
				popupGraph.bButton:setFillColor(unpack(commands.defColor))
				popupGraph.bButton.anchorX=0
				popupGraph.bButton.anchorY=0
				popupGraph.bButton.x=100
				popupGraph.bButton.y=graphElements.mainView.height + 110 + graphElements.txt_welcome.height
				popupGraph.bButton:addEventListener("tap", function(event)
					for k,v in pairs(popupGraph) do
						popupGraph[k]:removeSelf();
						popupGraph[k] = nil;

					end
					return true
				end)

				popupGraph.delButton  = display.newText({
					text = "Delete",
					x = 0,
					y = 0,
					font = composer.font,
					fontSize = commands.fontSize,
					align = "left",
					width = 430,
					height = 90
				})
				sceneGroup:insert(popupGraph.bButton)
				popupGraph.delButton.iter = event.target.iter
				popupGraph.delButton:setFillColor(unpack(commands.defColor))
				popupGraph.delButton.anchorX=1
				popupGraph.delButton.anchorY=0
				popupGraph.delButton.x=950
				popupGraph.delButton.y=graphElements.mainView.height + 110 + graphElements.txt_welcome.height
				popupGraph.delButton:addEventListener("tap", function(event)
					table.remove(commands.notes,event.target.iter)
					for k,v in pairs(gnotes) do
						gnotes[k]:removeSelf();
						gnotes[k] = nil;

					end

					for k,v in pairs(separators) do
						separators[k]:removeSelf();
						separators[k] = nil;

					end
					for k,v in pairs(popupGraph) do
						popupGraph[k]:removeSelf();
						popupGraph[k] = nil;

					end
					drawNotes(sceneGroup)
					return true
				end)

			end);

			graphElements.mainView:insert(obj);
			gnotes[k] = obj;

			local obj = display.newRect(display.contentWidth/2, len-19, 3*display.contentWidth/4, 3)
			obj.anchorX = 0
			obj.x = 10
			obj:setFillColor(unpack(commands.defColor))
			graphElements.mainView:insert(obj);
			separators[iter]=obj;
			iter = iter + 1;

		end
		separators[iter-1]:removeSelf();
		separators[iter-1]=nil;

	end
end

-- "scene:show()"
function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		audio.resume();
		drawNotes(sceneGroup);
		native.setKeyboardFocus(nil)
		-- Called when the scene is still off screen (but is about to come on screen).
	elseif ( phase == "did" ) then
	-- Called when the scene is now on screen.
	-- Insert code here to make the scene come alive.
	-- Example: start timers, begin animation, play audio, etc.
	end
end

-- "scene:hide()"
function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase

	commands.globalSave();

	if ( phase == "will" ) then
		audio.pause();
	-- Called when the scene is on screen (but is about to go off screen).
	-- Insert code here to "pause" the scene.
	-- Example: stop timers, stop animation, stop audio, etc.
	elseif ( phase == "did" ) then
		--force purge the scene, or move creation/destruction to show/hide, because of artifacts still left
		for k,v in pairs(gnotes) do
			gnotes[k]:removeSelf();
			gnotes[k] = nil;

		end

		for k,v in pairs(separators) do
			separators[k]:removeSelf();
			separators[k] = nil;

		end

		for k,v in pairs(graphElements) do
			graphElements[k]:removeSelf();
			graphElements[k] = nil;

		end
	end
end

-- "scene:destroy()"
function scene:destroy( event )

	local sceneGroup = self.view
	-- Called prior to the removal of scene's view ("sceneGroup").
	-- Insert code here to clean up the scene.
	-- Example: remove display objects, save state, etc.


end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
