local composer = require( "composer" )
local json = require "json"
local widget = require("widget")
local audio = require("audio");

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------

local scripts, commands, file, mailList, once

local txt_welcome, txt_mails, separators, mainView, menuButton, downloadButton, scv, downloadSplash, bcg

local populateMails, readMail, purgeMail, parseMail

local todo_incent = 0;

purgeMail = function()
	for k,v in pairs(txt_mails) do
		txt_mails[k]:removeSelf();
		txt_mails[k] = nil;

	end
	txt_mails = nil;
	txt_mails = {};

	for k,v in pairs(separators) do
		separators[k]:removeSelf();
		separators[k] = nil;

	end
	separators = nil;
	separators = {};
end

parseMail = function(target, table)
	local text
	--TODO
	if target == "from" then

	elseif target == "to" then

	elseif target == "date" then
		if commands.dates[table[2]] == nil then
			text = "??/??/?? ??:??:??"

		else
			local dateOffset = commands.dates[table[2]] + 245489458 - table[3]
			text = os.date( "%c", dateOffset )

		end

	elseif target == "text" then

	end

	return text
end

readMail = function(key)
	purgeMail()
	menuButton:removeEventListener("tap", menuButton)
	menuButton:removeSelf();
	menuButton = nil;
	menuButton  = display.newText({
		text = "Back",
		x = 0,
		y = 0,
		font = composer.font,
		fontSize = commands.fontSize,
		align = "left",
		width = 430,
		height = 90
	})
	scv:insert(menuButton)
	menuButton:setFillColor(unpack(commands.defColor))
	menuButton.anchorX=0
	menuButton.anchorY=0
	menuButton.x=100
	menuButton.y=mainView.height + 110 + txt_welcome.height
	function menuButton:tap(event) populateMails(); end
	menuButton:addEventListener("tap", menuButton)

	if mailList[key].Script ~= nil then
		scripts[mailList[key].Script[1]]("mail");
	end

	local len = 0;

	local obj, txt;
	if mailList[key].From[1] == "var" then
		txt = parseMail("from", mailList[key].From)

	else
		txt = mailList[key].From[1];

	end
	obj = display.newText({
		text = "From:    \t" .. txt ,
		x = 0,
		y = 0,
		font = composer.font,
		fontSize = commands.fontSize,
		align = "left",
		width = 760,
		height = 0
	})
	obj:setFillColor(unpack(commands.defColor))
	obj.anchorX=0
	obj.anchorY=0
	obj.x=10
	obj.y=len

	len = len + obj.height + 40;
	mainView:insert(obj);
	txt_mails["From"] = obj;

	if mailList[key].To[1] == "var" then
		txt = parseMail("to", mailList[key].To)

	else
		txt = mailList[key].To[1];

	end
	obj = display.newText({
		text = "To:      \t" .. txt ,
		x = 0,
		y = 0,
		font = composer.font,
		fontSize = commands.fontSize,
		align = "left",
		width = 760,
		height = 0
	})
	obj:setFillColor(unpack(commands.defColor))
	obj.anchorX=0
	obj.anchorY=0
	obj.x=10
	obj.y=len

	len = len + obj.height + 40;
	mainView:insert(obj);
	txt_mails["To"] = obj;

	if mailList[key].Date[1] == "var" then
		txt = parseMail("date", mailList[key].Date)

	else
		txt = mailList[key].Date[1];

	end
	obj = display.newText({
		text = "Date:    \t" .. txt,
		x = 0,
		y = 0,
		font = composer.font,
		fontSize = commands.fontSize,
		align = "left",
		width = 760,
		height = 0
	})
	obj:setFillColor(unpack(commands.defColor))
	obj.anchorX=0
	obj.anchorY=0
	obj.x=10
	obj.y=len

	len = len + obj.height + 40;
	mainView:insert(obj);
	txt_mails["Date"] = obj;


	if mailList[key].Text[1] == "var" then
		txt = parseMail("text", mailList[key].Date)

	else
		txt = mailList[key].Text[1];

	end
	obj = display.newText({
		text = txt ,
		x = 0,
		y = 0,
		font = composer.font,
		fontSize = commands.fontSize,
		align = "left",
		width = 760,
		height = 0
	})
	obj:setFillColor(unpack(commands.defColor))
	obj.anchorX=0
	obj.anchorY=0
	obj.x=10
	obj.y=len

	len = len + obj.height + 40;
	mainView:insert(obj);
	txt_mails["Text"] = obj;
	if once == nil then
		--commands.answer("")
		once = 1;
	end
	commands.answer("mail: Mail \"" .. key .. "\" opened.");
	todo_incent = 1;
	--TODO

	commands.hiddensetvar({"setvar", "lastmail", txt})

	if mailList[key].Attachment ~= nil then

		downloadButton  = display.newText({
			text = "Download",
			x = 0,
			y = 0,
			font = composer.font,
			fontSize = commands.fontSize,
			width = 430,
			height = 90
		})
		scv:insert(downloadButton)
		downloadButton:setFillColor(unpack(commands.defColor))
		downloadButton.anchorX=1
		downloadButton.anchorY=0
		downloadButton.x=950
		downloadButton.y=mainView.height + 110 + txt_welcome.height
		function downloadButton:tap(event)
			if mailList[key].Script ~= nil then
				scripts[mailList[key].Script[1]]("download");
			end
			local path = "data/assets/attachments/" .. mailList[key].Attachment[1] .. ".json"
			path = system.pathForFile(path, system.ResourceDirectory);
			local file = io.open(path, "r")
			local jsonFile = file:read("*a")
			local t = json.decode( jsonFile )
			io.close(file)

			local curr = commands.datStuct
			for index,value in ipairs(commands.activeDir) do curr = curr[value] end

			if curr.download == nil then
				curr.download = {}
				curr.download.files = {}
				curr = curr.download

			else
				curr = curr.download

			end

			local mdtext = "File already exists!"

			table.insert(commands.activeDir, "download")
			if not commands.isFile_full(mailList[key].Attachment[1], commands.activeDir) then
				table.insert(curr.files,mailList[key].Attachment[1])

				commands.fileList[commands.filePath(mailList[key].Attachment[1])] = t;

				commands.answer("mail: Attachment " .. mailList[key].Attachment[1] .. " downloaded.");
				todo_incent = 1;

				mdtext = "Download complete!"

			end
			table.remove(commands.activeDir)

			--TODO

			if downloadSplash == nil then
				downloadSplash = {}

				downloadSplash.back = display.newRect(display.contentWidth/2, display.contentHeight/2, 3000, 3000)
				downloadSplash.back:setFillColor(unpack(commands.bcgColor))

				downloadSplash.text = display.newText({
					text = mdtext,
					x = display.contentWidth/2,
					y = display.contentHeight/2,
					font = composer.font,
					fontSize = commands.fontSize,
				})
				downloadSplash.text:setFillColor(unpack(commands.defColor))

				downloadSplash.back:addEventListener("tap", function(event)
					downloadSplash.back:removeSelf()
					downloadSplash.text:removeSelf()
					downloadSplash.back = nil
					downloadSplash.text = nil
					downloadSplash = nil

					return true

				end)

				scv:insert(downloadSplash.back)
				scv:insert(downloadSplash.text)

			end

			return true
		end
		downloadButton:addEventListener("tap", downloadButton)

	end

end


populateMails = function()
	purgeMail()

	if downloadButton ~= nil then
		downloadButton:removeEventListener("tap", menuButton)
		downloadButton:removeSelf();
		downloadButton = nil;

	end

	menuButton:removeEventListener("tap", menuButton)
	menuButton:removeSelf();
	menuButton = nil;
	menuButton  = display.newText({
		text = "Close",
		x = 0,
		y = 0,
		font = composer.font,
		fontSize = commands.fontSize,
		anchorX = 0,
		width = 430,
		height = 90
	})
	scv:insert(menuButton)
	menuButton:setFillColor(unpack(commands.defColor))
	menuButton.anchorX=0
	menuButton.anchorY=0
	menuButton.x=100
	menuButton.y=mainView.height + 110 + txt_welcome.height
	function menuButton:tap(event)
		local options =
			{
				effect = "fade",
				time = 100,
			}
		if once == nil then
			commands.answer("")
			once = 1;
		end
		--commands.incent();
		composer.gotoScene( "main-scene", options )
		return true end
	menuButton:addEventListener("tap", menuButton)

	local len = 0;
	local iter = 1;

	if mailList ~= nil then
		for k,v in pairs(mailList) do
			local obj = display.newText({
				text = k,
				x = 0,
				y = 0,
				font = composer.font,
				fontSize = commands.fontSize,
				align = "left",
				width = 740,
				height = 0
			})
			obj:setFillColor(unpack(commands.defColor))
			obj.anchorX=0
			obj.anchorY=0
			obj.x=10
			obj.y=len

			len = len + obj.height + 41--+ obj.height/2;

			obj:addEventListener( "tap", function(event)
				readMail(k);

			end )

			mainView:insert(obj);
			txt_mails[k] = obj;

			local obj = display.newRect(display.contentWidth/2, len-19, 3*display.contentWidth/4, 3)
			obj.anchorX = 0
			obj.x = 10
			obj:setFillColor(unpack(commands.defColor))
			mainView:insert(obj);
			separators[iter]=obj;
			iter = iter + 1;

		end
		separators[iter-1]:removeSelf();
		separators[iter-1]=nil;

	end

	if (len == 0) then
		local obj = display.newText({
			text = "Archive is empty! :(",
			x = 0,
			y = 0,
			font = composer.font,
			fontSize = commands.fontSize,
			align = "left",
			width = 760,
			height = 0
		})
		obj:setFillColor(unpack(commands.defColor))
		obj.anchorX=0
		obj.anchorY=0
		obj.x=10
		obj.y=len

		len = len + obj.height + 41-- obj.height/2;
		mainView:insert(obj);
		txt_mails["emp"] = obj;
	end

end


-- "scene:create()"
function scene:create( event )

	local sceneGroup = self.view

	scv = sceneGroup
	scripts = event.params.scripts
	commands = event.params.scripts.coms
	file = event.params.file

	bcg = display.newRect(sceneGroup, 400, 635, 1200, 1800)
	bcg:setFillColor(unpack(commands.bcgColor))

	-- Initialize the scene here.
	-- Example: add display objects to "sceneGroup", add touch listeners, etc.

	txt_welcome  = display.newText({
		text = "Welcome to C&B certified mail system!\nEnjoy your stay!",
		x = 0,
		y = 0,
		font = composer.font,
		fontSize = commands.fontSize,
		align = "left",
		width = 740,
		height = 0
	})
	sceneGroup:insert(txt_welcome)
	txt_welcome:setFillColor(unpack(commands.defColor))
	txt_welcome.anchorX=0
	txt_welcome.anchorY=0
	txt_welcome.x=10
	txt_welcome.y=10

	mainView =  widget.newScrollView
		{
			top = 50 + txt_welcome.height,
			left = 20,
			width = 760,
			height = 1150 - 50 - txt_welcome.height,
			hideBackground = false,
			backgroundColor = commands.bcgColor,
			horizontalScrollDisabled = true,
			verticalScrollDisabled = false,
			isBounceEnabled = false
	}
	sceneGroup:insert(mainView)


	local path = "data/assets/mail/" .. file .. ".json"
	path = system.pathForFile(path, system.ResourceDirectory);
	if path~= nil then
		local file = io.open(path, "r")
		local jsonFile = file:read("*a")
		local t = json.decode( jsonFile )
		io.close(file)
		mailList = t
	end

	txt_mails  = {}
	separators = {}

	menuButton  = display.newText({
		text = "",
		x = 0,
		y = 0,
		font = composer.font,
		fontSize = commands.fontSize,
		align = "left",
		width = 430,
		height = 90
	})
	sceneGroup:insert(menuButton)
	menuButton:setFillColor(unpack(commands.defColor))
	menuButton.anchorX=0
	menuButton.anchorY=0
	menuButton.x=100
	menuButton.y=mainView.height + 110 + txt_welcome.height
	function menuButton:tap(event)  end
	menuButton:addEventListener("tap", menuButton)

	populateMails();

end


-- "scene:show()"
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Called when the scene is still off screen (but is about to come on screen).
		audio.resume();
		native.setKeyboardFocus(nil)

	elseif ( phase == "did" ) then
	-- Called when the scene is now on screen.
	-- Insert code here to make the scene come alive.
	-- Example: start timers, begin animation, play audio, etc.
	end
end


-- "scene:hide()"
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	commands.globalSave();

	if ( phase == "will" ) then
		-- Called when the scene is on screen (but is about to go off screen).
		-- Insert code here to "pause" the scene.
		-- Example: stop timers, stop animation, stop audio, etc.
		audio.pause();
		if todo_incent == 1 then
			todo_incent = 0;
			commands.incent();
		end
		
	elseif ( phase == "did" ) then

	end
end


-- "scene:destroy()"
function scene:destroy( event )

	local sceneGroup = self.view

	purgeMail();
	txt_mails = nil;
	separators = nil;
	txt_welcome:removeSelf();
	txt_welcome = nil
	mainView:removeSelf();
	mainView = nil
	bcg:removeSelf();
	bcg = nil;
	menuButton:removeSelf();
	menuButton=nil;
	file = nil;
	mailList = nil;
	once = nil;
	commands.globalSave();

-- Called prior to the removal of scene's view ("sceneGroup").
-- Insert code here to clean up the scene.
-- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
