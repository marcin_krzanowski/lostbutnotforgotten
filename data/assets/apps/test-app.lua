local composer = require( "composer" )
local json = require "json"
local widget = require("widget")
local audio = require("audio");

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------

local scripts, commands, custom, once

local graphElements

local closeScene = function(event)
	local options =
		{
			effect = "fade",
			time = 100,
		}
	if once == nil then
		commands.answer("")
		once = 1;
	end
	--commands.incent();
	composer.gotoScene( "main-scene", options )
	return true end


-- "scene:create()"
function scene:create( event )

	local sceneGroup = self.view

	scripts = event.params.scripts
	commands = event.params.scripts.coms
	custom = event.params.custom

	graphElements = {}

	-- Initialize the scene here.
	-- Example: add display objects to "sceneGroup", add touch listeners, etc.


	graphElements.bcg = display.newRect(sceneGroup, display.contentWidth/2, display.contentHeight/2, 3000, 3000)
	graphElements.bcg:setFillColor(unpack(commands.bcgColor))

	graphElements.bcg:addEventListener("tap", closeScene)

	graphElements.mainText = display.newText({
		parent = sceneGroup,
		text = "This is a test.\nCilck anywhere to exit;",
		x = 300,
		y = 200,
		font = composer.font,
		fontSize = commands.fontSize,
		align = "left"  --new alignment parameter
	})
	graphElements.mainText:setFillColor(unpack(commands.defColor))

end


-- "scene:show()"
function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
	-- Called when the scene is still off screen (but is about to come on screen).
		audio.resume();
		native.setKeyboardFocus(nil)
	
	elseif ( phase == "did" ) then
	-- Called when the scene is now on screen.
	-- Insert code here to make the scene come alive.
	-- Example: start timers, begin animation, play audio, etc.
	end
end


-- "scene:hide()"
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase
	
	commands.globalSave();
	
	if ( phase == "will" ) then
	-- Called when the scene is on screen (but is about to go off screen).
	-- Insert code here to "pause" the scene.
	-- Example: stop timers, stop animation, stop audio, etc.
		audio.pause();
		
	elseif ( phase == "did" ) then
		
	end
end


-- "scene:destroy()"
function scene:destroy( event )

	local sceneGroup = self.view

	-- Called prior to the removal of scene's view ("sceneGroup").
	-- Insert code here to clean up the scene.
	-- Example: remove display objects, save state, etc.
	
		for k,v in pairs(graphElements) do
			graphElements[k]:removeSelf();
			graphElements[k] = nil;

		end
	
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
