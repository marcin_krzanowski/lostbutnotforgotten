	application = 
	{
		content = 
		{ 
			width = 800,
			height = 1280,
			scale = "letterbox",
			fps = 60,
			xAlign = "center",
	        yAlign = "center",
			imageSuffix = {
				["@2x"] = 2,
			}
		}
	}

