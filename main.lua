-- hide the status bar
display.setStatusBar( display.HiddenStatusBar )

--random seed
math.randomseed(os.time());

--init composer
local composer = require( "composer" )

-- assign the ads plugin to the variable of the composer
--composer.ads = require("ads");

--set font, remove on final build
if "Win" == system.getInfo( "platformName" ) then
	--composer.font = "Inconsolata"
	composer.font = "Arial"
elseif "Android" == system.getInfo( "platformName" ) then
	composer.font = "IcsaR"
end

--start game
composer.gotoScene( "main-scene" )
