local composer = require( "composer" )
local audio = require("audio");

local scene = composer.newScene()

local colBlu = {68/255, 171/255, 215/255}
local colGold = {247/255, 188/255, 30/255}
local colRed = {229/255, 50/255, 21/255}
local colGray = {0.5, 0.5, 0.5}
local colBlack = {0, 0, 0}
local _W = display.contentWidth
local _H = display.contentHeight

local widget = require("widget")
local commands = require("commands")
commands.composer = composer;

local txtField, chat, mainView, keyPop, bound, bcg, scrollListener

local numComBack

local backgroundNiose, sndTable;

local clearView, createView, GlsceneGroup

local adsHeight = 0
local offset = 0

-- -----------------------------------------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE unless "composer.removeScene()" is called.
-- -----------------------------------------------------------------------------------------------------------------

-- local forward references should go here

-- -------------------------------------------------------------------------------

local function onKeyEvent( event )
	if event.phase == "down" then
		if event.keyName == "up" then
			txtField:dispatchEvent( { name = "userInput", phase = "up", text = txtField.text,
				newCharacters = txtField.text, oldText = txtField.text, startPosition = 0} )
			native.setKeyboardFocus(txtField)
			return true

		end

		if event.keyName == "down" then
			txtField:dispatchEvent( { name = "userInput", phase = "down", text = txtField.text,
				newCharacters = txtField.text, oldText = txtField.text, startPosition = 0} )
			native.setKeyboardFocus(txtField)
			return true

		end

		if event.keyName == "enter" then
			native.setKeyboardFocus(txtField)
			return true

		end

	end

	return false
end

function scrollListener( event )
	local x, y = mainView:getContentPosition()

	local phase = event.phase
	if ( phase == "began" ) then
	elseif ( phase == "moved" ) then
	elseif ( phase == "ended" ) then
	end

	-- In the event a scroll limit is reached...

	if ( event.direction == "up" ) then 
		if ( event.limitReached ) or y < -12000 then 
			if chat.lastSign < #commands.chatText then
				local shownText = commands.chatText

				chat.text = shownText

				local firstS, lastS

				while (chat.height + bound.height) > 24000 do
					lastS = chat.lastSign + #shownText/4
					if lastS > #commands.chatText then lastS = #commands.chatText end
					firstS = lastS - #shownText/2

					shownText = string.sub( commands.chatText, firstS, lastS)
					chat.text = shownText

				end

				while (chat.height + bound.height) < 1000 and #shownText < #commands.chatText do
					lastS = lastS + 20
					if lastS > #commands.chatText then lastS = #commands.chatText end
					firstS = firstS - 20

					shownText = string.sub( commands.chatText, firstS, lastS)
					chat.text = shownText

				end

				chat.firstSign = firstS
				chat.lastSign = lastS

				mainView:scrollToPosition({y = -(chat.height + bound.height)/2, time = 0})

			end
		end
		
	elseif ( event.direction == "down" ) then 
		--check if limit is reached or close to it, y is from :getContentPosition(), -800 is arbitrary
		if ( event.limitReached ) or y > - 800 then 
			--firstS stores number first visible sign of a string
			if chat.firstSign > 1 then
				--copy string
				local shownText = commands.chatText
				--update display object 
				chat.text = shownText

				local firstS, lastS
				
				--do while display object is too high - I found that scrollView stops scrolling ~25000 height
				while (chat.height + bound.height) > 24000 do
					--basically, cut the text in half
					firstS = chat.firstSign - #shownText/4
					if firstS < 1 then firstS = 1 end
					lastS = firstS + #shownText/2

					shownText = string.sub( commands.chatText, firstS, lastS)
					chat.text = shownText

				end

				while (chat.height + bound.height) < 1000 and #shownText < #commands.chatText do
					firstS = firstS - 20
					if firstS < 1 then firstS = 1 end
					lastS = lastS + 20

					shownText = string.sub( commands.chatText, firstS, lastS)
					chat.text = shownText

				end

				chat.firstSign = firstS
				chat.lastSign = lastS
				--scroll to halfway position in no time for seamless transition
				mainView:scrollToPosition({y = -(chat.height + bound.height)/2, time = 0})

			end

		end
	end

	return true
end


local updChat = function()
	if chat ~= nil then
		clearView()
		createView(GlsceneGroup)

		if (mainView.height>chat.height+bound.height) then
			--chat.y=mainView.height
			bound.y = mainView.height-bound.height-chat.height
			chat.y=bound.y+bound.height
			--mainView:setScrollHeight(chat.height+bound.height)
			mainView:scrollTo("top", { time = 100 })

		else
			chat.y=bound.height
			--chat.y=chat.height
			--mainView:setScrollHeight(chat.height+bound.height)
			mainView:scrollTo( "bottom", { time=100 } )


		end

	end
end

commands.ask = function(say)
	local T = say .."\n"
	commands.chatText = commands.chatText .. T

end

commands.incent = function()
	local T =  table.concat(commands.activeDir, "::") .. ">>"
	commands.chatText = commands.chatText .. T

end

commands.answer = function(say, pre)
	if pre == nil then pre = "" end
	commands.chatText = commands.chatText .. pre ..  say  .."\n"

end


local function textListener( event )
	if not commands.locked then
		if ( event.phase == "began" ) then
			-- user begins editing text field

			if system.getInfo( "platformName" ) ~= "Win" then
				native.setKeyboardFocus(txtField)
			end

		elseif ( event.phase == "ended" or event.phase == "submitted" ) then
			-- text field loses focus
			-- do something with defaultField's text
			local str = txtField.text
			if str:len() == 0 then return end
			commands.globalSave()

			--to maintain focus
			if system.getInfo( "platformName" ) ~= "Win" then
				if txtField.text~="" then
					native.setKeyboardFocus(txtField)
				else
					native.setKeyboardFocus(nil)
				end
			end

			timer.performWithDelay(20, function()
				txtField.text=""
				local ch = math.random( 20 )
				if ch <= 10 then
					audio.play( sndTable[ch]);

				end

				--add input to command list
				table.insert(commands.commList, 1, str)
				if table.getn(commands.commList) > commands.commHistoryMaxSize then
					table.remove(commands.commList)
				end

				commands.ask(str)
				commands.run(str)
			end)

			timer.performWithDelay(40, function()
				commands.incent();

			end)

			timer.performWithDelay(100, function()
				updChat();

			end)

			numComBack = 0;
			return true

		elseif ( event.phase == "editing" ) then

		elseif ( event.phase == "up" ) then
			if numComBack == commands.commHistoryMaxSize or numComBack == table.getn(commands.commList)
			then
				txtField.text = commands.commList[numComBack]
				txtField:setSelection(#commands.commList[numComBack], #commands.commList[numComBack])

			else
				numComBack = numComBack + 1
				txtField.text = commands.commList[numComBack]
				txtField:setSelection(#commands.commList[numComBack], #commands.commList[numComBack])

			end

			return true
		elseif ( event.phase == "down" ) then
			if numComBack == 0 then
				txtField.text=""

			elseif numComBack == 1 then
				numComBack = 0
				txtField.text=""

			else
				numComBack = numComBack - 1
				txtField.text = commands.commList[numComBack]
				txtField:setSelection(#commands.commList[numComBack], #commands.commList[numComBack])

			end

			return true
		end
	end
end

local function assert(args)
	commands.ask(args)
	commands.run(args)
	commands.incent();
end

createView = function (sceneGroup)
	bcg:setFillColor(unpack(commands.bcgColor))

	bound = display.newRect(0,0,800,700)
	bound:setFillColor(unpack(colBlack))
	bound.alpha = 0
	bound.anchorX = 0
	bound.anchorY = 0
	bound.x = 0
	bound.y = 0

	local shownText = commands.chatText

	--init chat view
	chat = display.newText({
		text = shownText,
		x = 0,
		y = 0,
		font = composer.font,
		fontSize = commands.fontSize,
		align = "left",
		width = 750,
		height = 0
	})


	while (chat.height + bound.height) > 24000 do
		shownText = string.sub( shownText, -math.ceil(#shownText/2))
		chat.text = shownText

	end

	while (chat.height + bound.height) <1000 and #shownText < #commands.chatText do
		shownText = string.sub( commands.chatText, -#shownText-math.ceil(#shownText/4))
		chat.text = shownText

	end
	chat.firstSign = #commands.chatText-#shownText
	chat.lastSign = #commands.chatText

	chat:setFillColor(unpack(commands.defColor))
	chat.anchorX = 0
	chat.anchorY = 0
	chat.x = 0
	chat.y = chat.height + bound.height


	mainView =  widget.newScrollView
		{
			top = 0,
			left = 20,
			width = 760,
			height = 1100-offset,
			scrollHeight = chat.height,
			hideBackground = false,
			backgroundColor = commands.bcgColor,
			horizontalScrollDisabled = true,
			verticalScrollDisabled = false,
			isBounceEnabled = false,
			friction = 0.95,
			maxVelocity = 10,
			listener = scrollListener
	}

	mainView:insert(bound)
	mainView:insert(chat)
	sceneGroup:insert(mainView)

	keyPop = {}
	keyPop.bcg = display.newImage( sceneGroup,"data/assets/images/const/kpop_bcg.png",
		system.ResourceDirectory, 400, mainView.height + 210)

	keyPop.up = display.newRect(sceneGroup, 760, mainView.height +55,90, 90)
	keyPop.up.alpha = 0
	keyPop.up.isHitTestable = true
	function keyPop.up:tap ( event )
		--upwards arrow
		txtField:dispatchEvent( { name = "userInput", phase = "up", text = txtField.text,
			newCharacters = txtField.text, oldText = txtField.text, startPosition = 0} )
		native.setKeyboardFocus(txtField)

		return true
	end
	keyPop.up:addEventListener( "tap", keyPop.up)

	keyPop.upimg = display.newPolygon( sceneGroup, 760, mainView.height +55, {20,20, 0,-20, -20,20 })
	keyPop.upimg:setFillColor(unpack(commands.defColor))

	keyPop.down = display.newRect(sceneGroup, 760, mainView.height +160,90, 90)
	keyPop.down.alpha = 0
	keyPop.down.isHitTestable = true
	function keyPop.down:tap( event )
		--downwards arrow
		txtField:dispatchEvent( { name = "userInput", phase = "down", text = txtField.text,
			newCharacters = txtField.text, oldText = txtField.text, startPosition = 0} )
		native.setKeyboardFocus(txtField)

		return true
	end

	keyPop.down:addEventListener( "tap", keyPop.down)
	keyPop.downimg = display.newPolygon( sceneGroup, 760, mainView.height +146, {-20,-20, 0,20, 20,-20 })
	keyPop.downimg:setFillColor(unpack(commands.defColor))

	if commands.imgObj ~= nil then
		for i, v in ipairs(commands.imgObj) do
			v:toFront()

		end

	end

end

clearView = function ()
	if chat ~= nil then
		chat:removeSelf()
		chat = nil

	end

	if bound ~= nil then
		bound:removeSelf()
		bound = nil

	end

	if mainView ~= nil then
		mainView:removeSelf()
		mainView = nil

	end

	if keyPop ~= nil then
		keyPop.bcg:removeSelf()
		keyPop.bcg = nil

		keyPop.up:removeEventListener("tap", keyPop.up)
		keyPop.up:removeSelf()
		keyPop.up = nil

		keyPop.down:removeEventListener("tap", keyPop.down)
		keyPop.down:removeSelf()
		keyPop.down = nil

		keyPop.upimg:removeSelf()
		keyPop.upimg = nil

		keyPop.downimg:removeSelf()
		keyPop.downimg = nil

		keyPop = nil
	end

end

local function createTextField(sceneGroup)
	txtField = native.newTextField( 360, mainView.height + 115, 660, 100 )
	txtField:addEventListener( "userInput", textListener )
	txtField.autocorrectionType = "UITextAutocorrectionTypeNo"
	sceneGroup:insert( txtField )



end

local function clearTextField()
	if txtField ~= nil then
		txtField:removeEventListener( "userInput", textListener )
		txtField:removeSelf();
		txtField = nil;

	end



end


-- "scene:create()"
function scene:create( event )

	local sceneGroup = self.view
	commands.group = sceneGroup
	GlsceneGroup = sceneGroup

	commands.initConst()
	commands.globalLoad("savedata")


	bcg = display.newRect(sceneGroup, 400, 635, 1200, 1800)
	bcg:setFillColor(unpack(commands.bcgColor))

	backgroundNiose = audio.loadStream("data/assets/audio/const/bcg.mp3", system.ResourceDirectory);
	sndTable = {};
	for i = 1,10 do
		sndTable[i] = audio.loadSound( "data/assets/audio/const/noise" .. i .. ".wav", system.ResourceDirectory );

	end

	if "Win" == system.getInfo( "platformName" ) then

		commands.globalSave()
	end

	audio.play( backgroundNiose, { channel = 1, loops = -1});
	audio.setVolume( 0.08, { channel=1 } );
	audio.pause();

end

-- "scene:show()"
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Called when the scene is still off screen (but is about to come on screen).
		audio.resume();
		createView(sceneGroup)

		createTextField(sceneGroup)

		updChat()
		numComBack = 0;

	elseif ( phase == "did" ) then
		-- Called when the scene is now on screen.
		-- Insert code here to make the scene come alive.
		-- Example: start timers, begin animation, play audio, etc.
		composer.removeHidden()

		Runtime:addEventListener( "key", onKeyEvent )


	end
end

-- "scene:hide()"
function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase

	commands.globalSave()

	if ( phase == "will" ) then
		-- Called when the scene is on screen (but is about to go off screen).
		-- Insert code here to "pause" the scene.
		-- Example: stop timers, stop animation, stop audio, etc.

		audio.pause();
		clearView()
		clearTextField()

	elseif ( phase == "did" ) then
		-- Called immediately after scene goes off screen.
		Runtime:removeEventListener( "key", onKeyEvent )

	end
end


-- "scene:destroy()"
function scene:destroy( event )
	local sceneGroup = self.view
	commands.globalSave()
	-- Called prior to the removal of scene's view ("sceneGroup").
	-- Insert code here to clean up the scene.
	-- Example: remove display objects, save state, etc.
	audio.stop()
	audio.dispose(backgroundNiose);
	for i = 1,10 do
		audio.dispose(sndTable[i]);

	end

	if mainView ~= nil then
		for k,v in pairs(mainView) do
			mainView[k]:removeSelf();
			mainView[k] = nil;

		end
		mainView:removeSelf();
		mainView = nil;
	end
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene
