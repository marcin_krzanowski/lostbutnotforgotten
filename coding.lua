local T = {}

local charSize = 254
local amplitude = charSize/math.pi
local frequency = math.pi/charSize*8

local function MDOA1(x)
	--return math.floor((amplitude * math.log(math.sin(-frequency * x)+1.5) % 8 )+ (amplitude*(math.sin(x) % 8)))

	return math.floor((amplitude * math.log(math.sin(-frequency * x) + 1.5) % 5 ) +
		(amplitude*(math.sin(x) % 8)) - 10 * math.sin(frequency * x)^2);
--return math.ceil(((amplitude * math.sin(-frequency * x)) + (amplitude*(math.sin(x)))) % 8);

--return x

end

local function stringToInt(input)
	if type(input) == "number" then
		return input
	elseif type(input) == "string" then
		local output = 0
		for x = 1, #input do
			output = output + input:byte(x)
		end

		return output
	else
		return 0
	end
end

function T.encrypt(input, key)
	key = stringToInt(key)
	local output = ""
	for x = 1, #input do
		--output = output .. string.char(((input:byte(x)- 33 + MDOA1(x + key)) % 93) + 33)
		local t = ((input:byte(x) + MDOA1(x + key)) % charSize)
		if t == 32 then
			t = 0
		else
			t = t + 1
		end

		output = output .. string.char(t)
	end

	return output
end

function T.decrypt(input, key)
	key = stringToInt(key)

	local output = ""
	for x = 1, #input do
		--output = output .. string.char(((input:byte(x) - 33 - MDOA1(x + key)) % 93)+33)
		local t = input:byte(x)
		if t == 0 then
			t = ((32 - MDOA1(x + key)) % charSize)
		else
			t = ((t - 1 - MDOA1(x + key)) % charSize)
		end
		output = output .. string.char(t)
	end


	return output
end

return T
