local T = {}

local json = require "json"
local audio = require("audio");
local coding = require ("coding");
local scripts = require ("scripts");

scripts.coms = T;

local function shallowcopy(orig)
	local orig_type = type(orig)
	local copy
	if orig_type == 'table' then
		copy = {}
		for orig_key, orig_value in pairs(orig) do
			copy[orig_key] = orig_value
		end
	else -- number, string, boolean, etc
		copy = orig
	end
	return copy
end

local function flatten(list)
	if type(list) ~= "table" then return {list} end
	local flat_list = {}
	for _, elem in ipairs(list) do
		for _, val in ipairs(flatten(elem)) do
			flat_list[#flat_list + 1] = val
		end
	end
	return flat_list
end

local function Set (list) --used to check if an argument is in a given table
	local set = {}
	for _, l in ipairs(list) do set[string.lower(l)] = true end
	return set
end

local function Setf (list) --works like set, nut used for files as they can have upper an lower case
	local set = {}
	for _, l in ipairs(list) do set[l] = true end
	return set
end

local function getArgs(args)
	local args = {}
	local num

	num = 1
	for i in string.gmatch(args, "%S+") do
		args[num] = i
		num = num + 1
	end
	return args
end

local function getSetArgs(args)
	local args = getArgs(args)
	return Set(args)
end

local function isFile(name)
	local curr = T.datStuct

	for index,value in ipairs(T.activeDir) do curr = curr[value] end

	return Setf(curr.files)[name]

end

function T.isFile_full(name, dir)
	local curr = T.datStuct

	if dir[1] == nil then return false end

	for index,value in ipairs(dir) do
		if curr[value] == nil or value == "files" then return false end
		curr = curr[value]
	end

	return Setf(curr.files)[name]

end

local function isRootLocked(root)
	if T.dirList[root] ~= nil then
		if T.dirList[root].locked == 1 then return true end

	elseif T.hiddDirList[root] ~= nil then
		if T.hiddDirList[root].locked == 1 then return true end

	else
		return true

	end

	return false

end

function T.filePath(name)

	return table.concat(T.activeDir,"") .. name

end

function T.filePath_full(name, dir)

	return table.concat(dir,"") .. name

end

local function printName(path, name)

	if path[1] == nil and name == nil then
		return ""

	end

	if path[1] == nil then
		return name

	end

	if name == nil then
		return table.concat(path,"::")

	end

	local match = 1
	for i, v in ipairs(T.activeDir) do
		if v ~= path[i] then
			match = 0
			break
		end

	end

	if match == 1 then
		if table.getn(T.activeDir) == table.getn(path) then
			return name

		else
			return table.concat(path,"::", table.getn(T.activeDir)+1) .. "::" .. name

		end

	else
		return table.concat(path,"::") .. "::" .. name

	end

end

local function isRoot(root)
	local exists = false
	for k,v in pairs(T.dirList) do
		if k == root then
			exists = true
			break
		end
	end
	for k,v in pairs(T.hiddDirList) do
		if exists or k == root then
			exists = true
			break
		end
	end
	return exists
end

local function pathToGlobal(path, isDir)
	path = string.gsub(table.concat(path,"::"), "%:+", " ")

	local temptarget = {}

	for j in string.gmatch(path, "%S+") do
		table.insert(temptarget, j)

	end

	path = nil
	path = shallowcopy(temptarget)

	local tepstart = string.upper(tostring(path[1]))

	if isRoot(tepstart) and ( table.getn(path) > 1 or isDir ) then
		--was provided with global dir
		path[1] = string.upper(path[1])

	else
		--not provided with global, add current dir as head to target
		local num = 1
		for index,value in ipairs(T.activeDir) do table.insert(path,num,value) num = num+1 end

	end

	local nfound
	repeat
		nfound = true
		for i, v in ipairs(path) do
			--change ".." into valid paths
			if v == ".." then
				table.remove(path,i)
				table.remove(path,i-1)
				nfound = false

			end

		end

	until nfound

	for i, v in ipairs(path) do
		--change "help" into valid path
		if v == "help" then
			path[i] = "help_dir"

		end

	end

	return path

end

local function isEmpty(arg)
	local curr = T.datStuct

	if arg ~= nil then table.insert(T.activeDir,arg) end

	for index,value in ipairs(T.activeDir) do curr = curr[value] end

	local num=0
	for key,value in pairs(curr) do
		if key ~= "files" then
			num = num + 1
		else
			for key,value in pairs(curr["files"]) do
				num = num + 1
			end
		end
	end

	if arg ~= nil then table.remove(T.activeDir) end

	return num < 1

end

local function isEmpty_full(path, target)
	local curr = T.datStuct

	table.insert(path,target)

	for index,value in ipairs(path) do curr = curr[value] end

	local num=0
	for key,value in pairs(curr) do
		if key ~= "files" then
			num = num + 1
		else
			for key,value in pairs(curr["files"]) do
				num = num + 1
			end
		end
	end

	table.remove(path)

	return num < 1

end

T.answer = function(args) end
T.ask = function(args) end

T.lspre = ""
T.commHistoryMaxSize = 100

T.datStuct = {}
T.fileList = {}
T.varList = {}
T.dirList = {}
T.hiddDirList = {}
T.imgObj = {}
T.snd = {}
T.notes = {}
T.activeScripts = {}

T.activeDir = {"Z"}
T.chatText = ""
T.username = "USERNAME"
T.commHistory = {}
T.commList = {}
T.defColor = {247/255, 188/255, 30/255}
T.fontSize = 42

T.locked = false
T.wait = false

T.initConst = function()
	local path = "data/constant/coms.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	local file = io.open(path, "r")
	local jsonFile = file:read("*a")
	local t = json.decode( jsonFile )
	io.close(file)
	T.list = t
	T.listSet = Set(T.list)

	path = "data/constant/hiddComs.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	file = io.open(path, "r")
	jsonFile = file:read("*a")
	t = json.decode( jsonFile )
	io.close(file)
	T.hiddList = t
	T.hiddListSet = Set(T.hiddList)

	path = "data/constant/strings.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	file = io.open(path, "r")
	jsonFile = file:read("*a")
	t = json.decode( jsonFile )
	io.close(file)
	T.strings = t

	jsonFile = nil
	file = nil

end

T.globalLoad = function(str)

	local path = "data/" .. str .. "/struct.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	local file = io.open(path, "r")
	local jsonFile = file:read("*a")
	local t = json.decode( jsonFile )
	io.close(file)
	T.datStuct = t

	path = "data/" .. str .. "/files.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	file = io.open(path, "r")
	jsonFile = file:read("*a")
	t = json.decode( jsonFile )
	io.close(file)
	T.fileList = t

	path = "data/" .. str .. "/variables.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	file = io.open(path, "r")
	jsonFile = file:read("*a")
	t = json.decode( jsonFile )
	io.close(file)
	T.varList = t

	path = "data/" .. str .. "/dirList.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	file = io.open(path, "r")
	jsonFile = file:read("*a")
	t = json.decode( jsonFile )
	io.close(file)
	T.dirList = t

	path = "data/" .. str .. "/hiddDirList.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	file = io.open(path, "r")
	jsonFile = file:read("*a")
	t = json.decode( jsonFile )
	io.close(file)
	T.hiddDirList = t

	path = "data/" .. str .. "/notes.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	file = io.open(path, "r")
	jsonFile = file:read("*a")
	t = json.decode( jsonFile )
	io.close(file)
	T.notes = t

	path = "data/" .. str .. "/other.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	file = io.open(path, "r")
	jsonFile = file:read("*a")
	t = json.decode( jsonFile )
	io.close(file)

	T.activeDir = t.activeDir
	T.fontSize = t.fontSize[1]
	T.chatText = t.chatText[1]
	T.username = t.username[1]
	T.commHistory = t.commHistory
	T.commList = t.commList
	T.bcgColor = t.bcgColor
	T.defColor = t.defColor
	T.activeScripts = t.activeScripts

	if (t.dates == nil) then
		T.dates = {}
		T.dates[1] = os.time()
	else
		T.dates = t.dates
	end

	jsonFile = nil
	file = nil

end

T.globalSave = function()

	local path = "data/savedata/struct.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	local file = io.open(path, "w")
	local t = T.datStuct
	local jsonFile = json.encode( t )
	file:write(tostring(jsonFile))
	io.close(file)

	path = "data/savedata/files.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	file = io.open(path, "w")
	t = T.fileList
	jsonFile = json.encode( t )
	file:write(tostring(jsonFile))
	io.close(file)

	path = "data/savedata/variables.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	file = io.open(path, "w")
	t = T.varList
	jsonFile = json.encode( t )
	file:write(tostring(jsonFile))
	io.close(file)

	path = "data/savedata/dirList.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	file = io.open(path, "w")
	t = T.dirList
	jsonFile = json.encode( t )
	file:write(tostring(jsonFile))
	io.close(file)

	path = "data/savedata/hiddDirList.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	file = io.open(path, "w")
	t = T.hiddDirList
	jsonFile = json.encode( t )
	file:write(tostring(jsonFile))
	io.close(file)

	path = "data/savedata/notes.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	file = io.open(path, "w")
	t = T.notes
	jsonFile = json.encode( t )
	file:write(tostring(jsonFile))
	io.close(file)

	path = "data/savedata/other.json"
	path = system.pathForFile(path, system.ResourceDirectory);
	file = io.open(path, "w")
	t = {}
	t.fontSize = {}
	t.fontSize[1] = T.fontSize
	t.activeDir = T.activeDir
	t.chatText = {}
	t.chatText[1] = T.chatText
	t.username = {}
	t.username[1] = T.username
	t.dates = T.dates
	t.commHistory = T.commHistory
	t.commList = T.commList
	t.defColor = T.defColor
	t.bcgColor = T.bcgColor
	t.activeScripts = T.activeScripts
	jsonFile = json.encode( t )
	file:write(tostring(jsonFile))
	io.close(file)

	jsonFile = nil
	file = nil

end


T.run = function(str)
	str = tostring(str)
	--removes chat head if it exceeds certain length
	if string.len(T.chatText) > 15000 then
		while string.len(T.chatText) > 14900 do
			local s, n = string.find( T.chatText, "\n"  )

			T.chatText = string.sub( T.chatText, -string.len(T.chatText) + n)

		end
		T.chatText = T.chatText
	end

	--str = string.lower(str)
	T.locked = true
	local args = {}
	local nums

	local num = 1
	for i in string.gmatch(str, "%S+") do
		args[num] = i
		num = num + 1
		if num > 2000 then break end
	end

	--this checks scripts before run
	local scriptResidu = {}
	for i,v in ipairs(T.activeScripts) do
		scriptResidu = scripts[v](args)

	end
	--TODO add more cases
	for i,v in ipairs(scriptResidu) do
		if v[1] == "done" then return v[2] end
	end

	--implemented to allow looping
	local iloops = {}
	local iovers = {}
	for index, value in ipairs(args) do
		if value == "loop" then
			table.insert(iloops,index)
		end
		if value == "over" then
			table.insert(iovers,index)
		end
	end

	local imax = table.getn(iloops)
	if imax > table.getn(iovers) then imax = table.getn(iovers) end

	for i = 1, imax do
		--reapeat, to allow continue in lua
		repeat
			local largs = ""
			local st = table.remove(iloops)
			local en = -1
			for i, v in ipairs(iovers) do
				if iovers[i] > st then
					en = table.remove(iovers,i)-st-1
				end
			end
			--continue statement
			if en == -1 then do break end end
			for j = 1, en do
				largs = largs .. " " .. table.remove(args, st+1)
			end

			table.insert(args, st+1, largs)
			for i, v in ipairs(iovers) do
				if iovers[i] > st then
					iovers[i] = v - en + 1
				end
			end
			for i, v in ipairs(iloops) do
				if iloops[i] > st then
					iloops[i] = v - en + 1
				end
			end
			--should terminate always
		until true
	end

	--implemented to allow conditions
	local iconds = {}
	local ithens = {}
	local ielses = {}
	for index, value in ipairs(args) do
		if value == "cond" then
			table.insert(iconds,index)
		end
		if value == "then" then
			table.insert(ithens,index)
		end
		if value == "else" then
			table.insert(ielses,index)
		end
	end

	local imax = table.getn(iconds)
	if imax > table.getn(ithens) then imax = table.getn(ithens) end

	for i = 1, imax do
		--reapeat, to allow continue in lua
		repeat
			local largs = ""
			local st = table.remove(iconds)
			local en = -1
			for i, v in ipairs(ithens) do
				if ithens[i] > st then
					en = table.remove(ithens,i)-st-1
				end
			end
			--continue statement
			if en == -1 then do break end end
			for j = 1, en do
				largs = largs .. " " .. table.remove(args, st+1)
			end

			table.insert(args, st+1, largs)
			for i, v in ipairs(ithens) do
				if ithens[i] > st then
					ithens[i] = v - en + 1
				end
			end
			for i, v in ipairs(iconds) do
				if iconds[i] > st then
					iconds[i] = v - en + 1
				end
			end
			for i, v in ipairs(ielses) do
				if ielses[i] > st then
					ielses[i] = v - en + 1
				end
			end

			--now, for the "then"-"else" statement
			st = st + 2
			en = -1
			for i, v in ipairs(ielses) do
				if ielses[i] > st then
					en = table.remove(ielses,i)-st-1
				end
			end
			--continue statement
			if en == -1 then do break end end

			largs = ""
			for j = 1, en do
				largs = largs .. " " .. table.remove(args, st+1)
			end

			table.insert(args, st+1, largs)
			for i, v in ipairs(ithens) do
				if ithens[i] > st then
					ithens[i] = v - en + 1
				end
			end
			for i, v in ipairs(iconds) do
				if iconds[i] > st then
					iconds[i] = v - en + 1
				end
			end
			for i, v in ipairs(ielses) do
				if ielses[i] > st then
					ielses[i] = v - en + 1
				end
			end

			--should terminate always
		until true
	end



	while Set(args)["("] do
		if not Set(args)[")"] then
			T.answer(T.strings.run[1])
			return -1;
		end
		local numo = -1
		local numc = -1
		for index, value in ipairs(args) do
			if value == "(" then
				numo = index
			end
			if value == ")" then
				numc = index
			end
			if numo ~= -1 and numc ~= -1 then
				if numo < numc then
					break
				else
					T.answer(T.strings.run[1])
					return -1;
				end
			end
		end
		local newargs = T.run(table.concat(args," ",numo+1,numc-1))

		for i = numo, numc do
			table.remove(args,numo)
		end

		if newargs ~= nil then
			for i in string.gmatch(newargs, "%S+") do
				table.insert(args,numo,i)
				numo = numo + 1
				if table.getn(args) > 2000 then break end
			end
		end
	end

	if Set(args)["|"] then
		local temp
		local tempargs = {}
		local tempargs2 = {}
		for index, value in ipairs(args) do
			if value == "|" then
				temp = index break
			end
		end
		table.remove(args, temp)
		for i = 1, table.getn(args) do
			if i<temp then
				table.insert(tempargs2, args[i])
			else
				table.insert(tempargs, args[i])
			end
		end
		temp = T.run(table.concat(tempargs, " "))
		if temp ~= nil then
			--temp = string.lower(temp)
			for i in string.gmatch(temp, "%S+") do
				table.insert(tempargs2, i)

			end
		end
		args = nil
		args = tempargs2
	end

	--return -1 if there is no command (no args) - it happens only ith empty () or |
	if args[1] == nil then
		return -1
	end

	local lcarg = string.lower(args[1]) --lowercase args[1] to allow use anycase in args
	if lcarg == "p" then
		if table.getn(args) == 1 then args[2] = 1 end
		if tonumber(args[2]) == nil then table.insert(args, 2, 1 ) end
		if table.getn(T.commHistory) >= tonumber(args[2]) then
			T.locked = false
			local largs = shallowcopy(args)
			table.remove(largs,1); table.remove(largs,1);

			if table.getn(largs) < 1 then largs = "" else largs = table.concat(largs, " ") end

			T.answer(T.commHistory[tonumber(args[2])] .. " " .. largs)

			return T.run(T.commHistory[tonumber(args[2])] .. " " .. largs)

		else
			T.answer(T.strings.run[2])

		end

	else
		if table.getn(T.commHistory) >= T.commHistoryMaxSize then
			table.remove(T.commHistory)
		end
		table.insert(T.commHistory,1,table.concat(args," "))

		if T.listSet[lcarg] or T.hiddListSet[lcarg] then
			T.locked = false
			return T[lcarg](args)

		elseif T.hiddListSet["_" .. lcarg] then
			T.locked = false
			return T["_" .. lcarg](args)

		else
			T.answer(T.strings.run[3] .. tostring(args[1]) .. T.strings.run[4])
			T.answer(T.strings.run[5])
			T.locked = false
			return -1

		end
	end
end

T.help = function(args)
	local setArgs = Set(args)

	if table.getn(args) == 1 then
		T.answer(T.strings.help[1])

		for index,value in ipairs(T.list) do T.answer(value) end
		return args[1]

	else
		if args[2] == "help" then
			T.answer("help: Are you trying to be smart?")
			T.answer("help: I mean, what do \"help\" does I wonder?")
			T.answer("help: Who doesn't know that?")
			T.answer("help: I mean, I had to write this clause ONLY because the types like you.")
			T.answer("help: Previously, the system just looped itself into oblivion.")
			T.answer("help: And not like TES one.")
			T.answer("help: I'd never have thought someone would do such a silly thing before.")
			T.answer("help: I mean, what is even the point in endless design, development and testing, "..
				"when all that is needed to break everything is one dummy.")
			T.answer("help: I hope you're proud of yourself.")
			return -1

		elseif args[2] == "no" then
			T.answer("no: negates the value or values.")
			T.answer("no: syntax no")
			T.answer("no: syntax no [arg1] [arg2] ...")
			T.answer("no: or whatever...")
			return "no"

		end

		if T.listSet[args[2]] then
			return T[args[2]]({args[2], "help"})

		elseif T.hiddListSet[args[2]] then
			return T[args[2]]({args[2], "help"})

		else
			T.answer(T.strings.help[2] .. args[2] .. T.strings.help[3])
			return args[1]

		end
	end
end

T.motd = function(args)

	local setArgs = Set(args)

	if setArgs["help"] then
		T.answer(T.strings.motd[1])
		return args[1]

	else
		local file = io.open(system.pathForFile("data/constant/motd.json", system.ResourceDirectory), "r")
		local listMOTD = json.decode( file:read("*a") )
		io.close(file)

		local msg = listMOTD[math.random(table.getn(listMOTD))]
		msg = string.gsub( msg, "USERNAME", T.username )
		T.answer(msg)
		return(msg)

	end
end

T.date = function(args)

	local setArgs = Set(args)

	if setArgs["help"] then
		T.answer(T.strings.date[1])
		return args[1]

	else
		local t = os.time() + 245489458
		T.answer(T.strings.date[2] .. os.date( "%c", t ))
		return t

	end
end

T.fontsize = function(args)

	local setArgs = Set(args)

	if setArgs["help"] then
		T.answer(T.strings.fontsize[1])
		return args[1]

	elseif table.getn(args) <= 1 then
		T.answer(T.strings.fontsize[2] .. T.fontSize .. ".")
		return T.fontSize

	else
		if tonumber(args[2]) ~= nil then
			if tonumber(args[2]) >= 10 and tonumber(args[2]) <=100 then
				T.fontSize = tonumber(args[2])
				T.answer(T.strings.fontsize[3] .. tonumber(args[2]) .. ".")
				return T.fontSize
			end

			T.answer(T.strings.fontsize[4] .. args[2] .. T.strings.fontsize[5])
			return tonumber(args[2])

		else
			T.answer(T.strings.fontsize[6] .. args[2] .. ".")
			return T.fontSize

		end
	end
end

T.question = function(args)

	local setArgs = Set(args)

	if setArgs["help"] then
		T.answer(T.strings.question[1])
		return args[1]

	else
		local file = io.open(system.pathForFile("data/constant/que.json", system.ResourceDirectory), "r")
		local listQue = json.decode( file:read("*a") )
		io.close(file)

		local msg = listQue[math.random(table.getn(listQue))]
		msg = string.gsub( msg, "USERNAME", T.username )
		T.answer(msg)
		return(msg)

	end
end

T.credits = function(args)

	local setArgs = Set(args)

	if setArgs["help"] then
		T.answer(T.strings.credits[1])
		return args[1]

	else
		T.answer(T.strings.credits[2])

		--TODO password
		return "credits: super secret super code"

	end
end

T.insult = function(args)

	local setArgs = Set(args)

	if setArgs["help"] then
		T.answer(T.strings.insult[1])
		return args[1]


	else
		local file = io.open(system.pathForFile("data/constant/insult.json", system.ResourceDirectory), "r")
		local listInsult = json.decode( file:read("*a") )
		io.close(file)

		local msg = listInsult[math.random(table.getn(listInsult))]
		msg = string.gsub( msg, "USERNAME", T.username )
		T.answer(msg)
		return(msg)
	end
end

T.name = function(args)

	local setArgs = Set(args)

	if setArgs["help"] then
		T.answer(T.strings.name[1])
		return args[1]

	elseif table.getn(args) == 1 then
		T.answer(T.strings.name[2] .. T.username)
		return T.username

	else
		T.username = tostring(args[2])
		T.answer(T.strings.name[2] .. T.username)
		return T.username

	end
end

T.yes = function(args)

	T.answer(T.strings.yes[1])
	return "y"

end

T.ruler = function(args)

	T.answer("ruler: Now, you can figure out the length of things easily.")
	return "ruler: This might actually be useful sometimes, I hope you understand this is just an easter egg."

end

T.protractor = function(args)

	T.answer("protractor: Now, you can fairly easily figure out the angle of various things.")
	return "An obligatory, obvious Earthbound reference."

end

T.repeats = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) == 1 then
		T.answer(T.strings.repeats[1])
		return args[1]

	else
		local num = tonumber(args[2])
		if num == nil then
			T.answer(T.strings.repeats[2] .. tostring(args[2]) .. T.strings.repeats[3])
			return -1

		else

			table.remove(args,1)
			table.remove(args,1)
			if args[1] == nil then
				T.answer(T.strings.repeats[4])
				return -1

			end
			if num < 500 then
				if num >= 50 then
					T.answer(T.strings.repeats[5])
				end
				local temp = ""
				local maxdone = 0
				for i=1,num do
					maxdone = maxdone + string.len(table.concat(args," "))
					if maxdone > 1000 then
						T.answer(T.strings.repeats[8])
						return -1
					end
					temp = temp .. table.concat(args," ") .. " "

				end
				T.answer(T.strings.repeats[7] .. temp)
				return temp
			else
				T.answer(T.strings.repeats[6])
				return -1

			end
		end
	end
end

T.hardreset = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) == 1 then
		T.answer(T.strings.hardreset[1])
		return args[1]

	else
		for i = 2,table.getn(args) do
			if args[i] ~= "y" then
				T.answer(T.strings.hardreset[2] .. args[i] .. "\".")
				T.answer(T.strings.hardreset[3] .. T.username ..".")
				return -1

			end
		end
		if table.getn(args) == 2 then
			T.answer(T.strings.hardreset[4])
			return -1

		elseif table.getn(args) == 3 then
			T.answer(T.strings.hardreset[5])
			return -1

		elseif table.getn(args) == 4 then
			T.answer(T.strings.hardreset[6])
			return -1

		elseif table.getn(args) == 5 then
			T.answer(T.strings.hardreset[7])
			return -1

		elseif table.getn(args) == 6 then
			T.globalLoad("origData")
			T.stop({"stop"})
			T.cls({"cls"})
			T.answer(T.strings.hardreset[8])
			T.globalSave()
			return 0

		elseif table.getn(args) >= 7 and table.getn(args) < 20 then
			T.answer(T.strings.hardreset[9])
			return -1

		elseif table.getn(args) >= 20 and table.getn(args) < 80 then
			T.answer(T.strings.hardreset[10])
			return -1

		elseif table.getn(args) >= 80 and table.getn(args) < 200 then
			T.answer(T.strings.hardreset[11])
			return -1

		elseif table.getn(args) >= 200 and table.getn(args) < 1000 then
			T.answer(T.strings.hardreset[12])
			return -1

		elseif table.getn(args) >= 1000 then
			T.answer(T.strings.hardreset[13])
			T.answer(T.strings.hardreset[14] .. T.strings.hardreset[15])
			T.answer(T.strings.hardreset[16])
			--TODO password
			return T.strings.hardreset[15]
		end
	end
end

T.ls = function(args)
	local function tree(key)
		local temp = shallowcopy(T.activeDir)
		table.insert(T.activeDir, key)
		local a = nil
		local b
		local rargs = ""
		if Set(args)["type"] then a = "type" end
		T.lspre = T.lspre .. "-"
		if not (isEmpty()) then
			rargs = rargs .. " " .. T.ls({"ls", "tree", a})
		else
			rargs = rargs .. " " .. T.ls({"ls", a})
		end
		if(string.len(T.lspre) == 1) then
			T.lspre = ""
		else
			T.lspre = string.sub(T.lspre,1,string.len(T.lspre)-1)
		end
		T.activeDir = shallowcopy(temp)
		return rargs
	end
	local retargs = ""
	local setArgs = Set(args)

	if setArgs["help"] then
		T.answer(T.strings.ls[1])
		return args[1]

	else

		local curr = T.datStuct
		for index,value in ipairs(T.activeDir) do curr = curr[value] end

		for key,value in pairs(curr) do
			if key ~= "files" then
				retargs = retargs .. " " .. key

				if setArgs["type"] then
					T.answer(key .. "   \tdir", T.lspre)

				else
					T.answer(key, T.lspre)

				end
				if setArgs["tree"] then
					retargs = retargs .. " " .. tree(key)

				end
			end
		end

		for index,value in ipairs(curr.files) do
			retargs = retargs .. " " .. value
			if setArgs["type"] then
				-- file types?
				T.answer(value .. "   \tfile", T.lspre)
			else
				T.answer(value, T.lspre)
			end
		end

		--returns dir list, to iterate over
		return retargs
	end
end

T.cd = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) == 1 then
		T.answer(T.strings.cd[1])
		return args[1]

	else

		if setArgs[".."] then
			if table.getn(T.activeDir) > 1 then
				table.remove(T.activeDir)
				--T.incent();

			else
				T.answer(T.strings.cd[2])
				return -1

			end

		else
			if table.getn(args) > 2 then
				args[2] = table.concat(args,"::",2)

			end

			local temp = args[2]
			local temp2 = args[2]
			local num = string.find(temp, "::")
			if num~=nil then
				temp = string.sub(args[2], 1, num-1)
				temp2 = string.sub(args[2], num+2, string.len(args[2]))
			end
			local curr = T.datStuct
			for index,value in ipairs(T.activeDir) do curr = curr[value] end
			if type(curr[temp]) == "table" and curr[temp] ~= "files" then
				table.insert(T.activeDir, temp)
				--T.incent();
				if num ~= nil then
					return temp .. " " .. T.cd({"cd", temp2})

				else
					return temp

				end

			else
				T.answer(T.strings.cd[3] .. args[2])
				return -1

			end
		end
	end
end


T.cp = function(args)
	local setArgs = Set(args)
	if setArgs["help"] or table.getn(args) == 1 then
		T.answer("cp: Copies file or directory to a new directory.")
		T.answer("cp: Type path in a regular fashion.")
		T.answer("cp: Like: dir1::dir2")
		T.answer("cp: Use \".\" to indicate current directory.")
		T.answer("cp: It's not too hard, just try it!")
		T.answer("cp: SYNTAX cp [source] [target]")
		return args[1]

	else
		local source = args[2]

		--check, if target exists
		local target
		if table.getn(args) >= 3 then
			target = shallowcopy(args)
			table.remove(target,1)
			table.remove(target,1)

		else
			target = "."

		end

		--change target to global
		if target == "." then
			target = shallowcopy(T.activeDir)

		elseif target == ".." then
			if table.getn(T.activeDir) > 1 then
				target = shallowcopy(T.activeDir)
				table.remove(target)

			else
				T.answer("cp: ERROR Already in root directory.")
				T.answer("cp: You probably typed too many dots...")
				return -1

			end

		else

			target = pathToGlobal(target, true)

			if isRootLocked(target[1]) then
				T.answer("cp: ERROR Root directory " .. target[1] .. " still locked.")
				T.answer("cp: You should try and unlock it first...")
				return -1

			end

		end

		--check if target exists
		local curr = T.datStuct
		for index,value in ipairs(target) do
			if value ~= "files" and curr[value] ~= nil then
				curr = curr[value]
			else
				T.answer("cp: ERROR Directory " .. printName(target) .. " does not exist!.")
				T.answer("cp: You should create it first...")
				return -1
			end

		end

		local filecopy = {}
		--check if source is a file

		local sourcepath = pathToGlobal({source})
		source = table.remove(sourcepath)

		if T.isFile_full(source, sourcepath) and not isRootLocked(sourcepath[1]) then
			if T.fileList[T.filePath_full(source, sourcepath)].script ~= nil then
				scripts[T.fileList[T.filePath_full(source, sourcepath)].script[1]]("cp");
			end

			for k, value in pairs(T.fileList[T.filePath_full(source, sourcepath)]) do
				filecopy[k] = {}
				for i, v in ipairs(value) do
					filecopy[k][i] = v
				end
			end

		else
			T.answer("cp: ERROR " .. printName(sourcepath, source) .. " is not a file!.")
			T.answer("cp: Sadly, you can copy only files...")
			return -1

		end

		local tempsource = source

		while T.isFile_full(source, target) do
			source = source .. "_cp"

		end

		local curr = T.datStuct
		for index,value in ipairs(target) do curr = curr[value] end

		table.insert(curr.files,source)
		T.fileList[T.filePath_full(source, target)]={};

		for k, value in pairs(filecopy) do
			T.fileList[T.filePath_full(source, target)][k]={};
			for i, v in ipairs(filecopy[k]) do
				T.fileList[T.filePath_full(source, target)][k][i] = v
			end
		end

		T.answer("cp: File \"" .. printName(sourcepath, tempsource) .. "\" copied to " .. printName(target, source) .. "!")
		return printName(target, source)

	end

end

T.mkfile = function(args)
	local setArgs = Set(args)
	if setArgs["help"] or table.getn(args) == 1 then
		T.answer(T.strings.mkfile[1])
		return args[1]
	else

		local target = {args[2]}
		target = pathToGlobal(target)
		local filename = table.remove(target)

		if target[1] == nil then
			T.answer("mkfile: ERROR Bad file path!")
			return -1

		end

		if isRootLocked(target[1]) then
			T.answer("mkfile: ERROR Cannot access root: " .. target[1])
			return -1

		end

		if string.lower(filename) == "files" or string.lower(filename) == ".." or string.lower(filename) == "." then
			T.answer(T.strings.mkfile[2] .. filename .. ".")
			return -1

		else
			if T.isFile_full(filename, target) then
				T.answer(T.strings.mkfile[3])
				return -1

			else

				local curr = T.datStuct
				for index,value in ipairs(target) do curr = curr[value] end

				table.remove(args, 1)
				table.remove(args, 1)
				local phrase = table.concat(args," ")
				table.insert(curr.files,filename)
				T.fileList[T.filePath_full(filename, target)]={};
				T.fileList[T.filePath_full(filename, target)].type={};
				T.fileList[T.filePath_full(filename, target)].type[1]="text";
				T.fileList[T.filePath_full(filename, target)].text={};
				T.fileList[T.filePath_full(filename, target)].text[1]=phrase;
				T.fileList[T.filePath_full(filename, target)].prog={};
				T.fileList[T.filePath_full(filename, target)].prog[1]="";


				T.fileList[T.filePath_full(filename, target)].img={};
				T.fileList[T.filePath_full(filename, target)].img[1] = "noise" .. math.random(9) .. ".png";
				T.fileList[T.filePath_full(filename, target)].img[2] = math.random(50, 500);
				T.fileList[T.filePath_full(filename, target)].img[3] = math.random(50, 500);

				T.fileList[T.filePath_full(filename, target)].audio={};
				T.fileList[T.filePath_full(filename, target)].audio[1]="noise" .. math.random(3) .. ".mp3";

				T.answer(T.strings.mkfile[4] .. printName(target, filename) .. T.strings.mkfile[5])

				return args[2]

			end
		end
	end
end

T.mkdir = function(args)
	local setArgs = Set(args)
	if setArgs["help"] or table.getn(args) == 1 then
		T.answer(T.strings.mkdir[1])
		return args[1]
	else

		table.remove(args, 1);

		local path = pathToGlobal(args)

		local dirname = table.remove(path)

		if path[1] == nil then
			T.answer("mkdir: ERROR Bad directory.")
			return -1

		end

		if isRootLocked(path[1]) then
			T.answer("mkdir: ERROR Root directory " .. path[1] .. " locked.")
			return -1

		end

		--check if target exists
		local curr = T.datStuct
		for index,value in ipairs(path) do
			if value ~= "files" and curr[value] ~= nil then
				curr = curr[value]
			else
				T.answer("mkdir: ERROR Directory " .. printName(path) .. " does not exist!.")
				return -1
			end

		end

		if string.lower(dirname) == "files" or string.lower(dirname) == ".." or string.lower(dirname) == "."
			or string.lower(dirname) == "help" then
			T.answer(T.strings.mkdir[2] .. dirname .. "\".")
			return -1

		else
			local curr = T.datStuct
			for index,value in ipairs(path) do curr = curr[value] end

			if type(curr[dirname]) == "table" then
				T.answer(T.strings.mkdir[3] .. dirname .. T.strings.mkdir[6])
				return -1

			else
				curr[dirname]={ ["files"]={} }
				T.answer(T.strings.mkdir[4] .. printName(path, dirname) .. T.strings.mkdir[5])
				return printName(path, dirname)

			end
		end
	end
end

T.rpsls = function(args)

	local setArgs = Set(args)

	if setArgs["help"] then
		T.answer("rpsls: Wow, you really need help with rock paper scissors lizard spock?")
		T.answer("rpsls: Maybe you should go back to that children game, rock paper scissors?")
		return args[1]

	elseif table.getn(args) == 1 then
		T.answer("rpsls: Rock paper scissors lizard spock")
		T.answer("rpsls: But you should already know that")
		T.answer("rpsls: Are we playing or what?")

	else
		args[2] = string.lower(args[2])

		local resp = math.random(5)

		if resp == 1 then
			T.answer("rpsls: Rock")

		elseif resp == 2 then
			T.answer("rpsls: Paper")

		elseif resp == 3 then
			T.answer("rpsls: Scissors")

		elseif resp == 4 then
			T.answer("rpsls: Lizard")

		else
			T.answer("rpsls: Spock")

		end

		local out = "rps"

		if args[2] == "rock" or args[2] == "r" then
			if resp == 1 then
				out = 0

			elseif resp == 2  then
				T.answer("rpsls: Paper covers rock...")
				out = -1

			elseif resp == 3 then
				T.answer("rpsls: Rock crushes scissors...")
				out = 1

			elseif resp == 4 then
				T.answer("rpsls: Rock crushes lizard...")
				out = 1

			else
				T.answer("rpsls: Spock vaporizes rock...")
				out = -1

			end

		elseif args[2] == "paper" or args[2] == "p" then
			if resp == 1 then
				T.answer("rpsls: Paper covers rock...")
				out = 1

			elseif resp == 2  then
				out = 0

			elseif resp == 3 then
				T.answer("rpsls: Scissors cuts paper...")
				out = -1

			elseif resp == 4 then
				T.answer("rpsls: Lizard eats paper...")
				out = -1

			else
				T.answer("rpsls: Paper disproves Spock...")
				out = 1

			end

		elseif args[2] == "scissors" or args[2] == "s" then
			if resp == 1 then
				T.answer("rpsls: Rock crushes scissors...")
				out = -1


			elseif resp == 2  then
				T.answer("rpsls: Scissors cuts paper...")
				out = 1

			elseif resp == 3 then
				out = 0

			elseif resp == 4 then
				T.answer("rpsls: Scissors decapitates lizard...")
				out = 1

			else
				T.answer("rpsls: Spock smashes scissors...")
				out = -1

			end
		elseif args[2] == "lizard" or args[2] == "l" then
			if resp == 1 then
				T.answer("rpsls: Rock crushes lizard...")
				out = -1


			elseif resp == 2  then
				T.answer("rpsls: Lizard eats paper...")
				out = 1


			elseif resp == 3 then
				T.answer("rpsls: Scissors decapitates lizard...")
				out = -1


			elseif resp == 4 then
				out = 0

			else
				T.answer("rpsls: Lizard poisons Spock...")
				out = 1

			end
		elseif args[2] == "spock" or args[2] == "sp" then
			if resp == 1 then
				T.answer("rpsls: Spock vaporizes rock...")
				out = 1

			elseif resp == 2  then
				T.answer("rpsls: Paper disproves Spock...")
				out = -1

			elseif resp == 3 then
				T.answer("rpsls: Spock smashes scissors...")
				out = 1

			elseif resp == 4 then
				T.answer("rpsls: Lizard poisons Spock...")
				out = -1

			else
				out = 0

			end
		end

		if out == 1 then
			T.answer("You win, scrub")

		elseif out == 0 then
			T.answer("A tie, ehhh")

		elseif out == -1 then
			T.answer("I win, whatever")

		else
			T.answer("rpsls: What? You don't know how to play?")
			T.answer("rpsls: I don't have time to teach you")
			T.answer("rpsls: Google it or smth")
			T.answer("rpsls: Or even better, go bother my cousin, rps")

		end

		return out

	end

end

T.rps = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) == 1 then
		T.answer("rps: Rock-Paper-Scissors!")
		T.answer("rps: Type \"rps [rock/paper/scissors]\" to play!")
		return args[1]

	else
		args[2] = string.lower(args[2])

		local resp = math.random(3)
		if resp == 1 then
			T.answer("rps: Rock!")

		elseif resp == 2 then
			T.answer("rps: Paper!")

		else
			T.answer("rps: Scissors!")

		end

		if args[2] == "rock" or args[2] == "r" then
			if resp == 1 then
				T.answer("rps: A tie!")
				return 0

			elseif resp == 2  then
				T.answer("rps: Paper covers rock!")
				T.answer("rps: You loose!")
				return -1

			else
				T.answer("rps: Rock crushes scissors!")
				T.answer("rps: You win!")
				return 1

			end

		elseif args[2] == "paper" or args[2] == "p" then
			if resp == 1 then
				T.answer("rps: Paper covers rock!")
				T.answer("rps: You win!")
				return 1

			elseif resp == 2  then
				T.answer("rps: A tie!")
				return 0

			else
				T.answer("rps: Scissors cuts paper!")
				T.answer("rps: You loose!")
				return -1

			end

		elseif args[2] == "scissors" or args[2] == "s" then
			if resp == 1 then
				T.answer("rps: Rock crushes scissors!")
				T.answer("rps: You loose!")
				return -1

			elseif resp == 2  then
				T.answer("rps: Scissors cuts paper!")
				T.answer("rps: You win!")
				return 1

			else
				T.answer("rps: A tie!")
				return 0

			end
		else
			T.answer("rps: Wow, you are sooooo sophisticated.")
			T.answer("rps: Maybe you should hang out with my cousin, rpsls.")
			T.answer("rps: He is way cooler than me.")
			return "rpsls"

		end

	end

end

T.echo = function(args)

	local setArgs = Set(args)

	if (table.getn(args) == 2 and setArgs["help"]) or table.getn(args) == 1 then
		T.answer(T.strings.echo[1])
		return args[1]

	else

		local target = shallowcopy(args)
		table.remove(target,1)

		target = pathToGlobal(target)
		local filename = table.remove(target)

		if T.isFile_full(filename, target) and not isRootLocked(target[1]) then
			--file exists, read
			if T.fileList[T.filePath_full(filename, target)].script ~= nil then
				scripts[T.fileList[T.filePath_full(filename, target)].script[1]]("echo");
			end

			if T.fileList[T.filePath_full(filename, target)].locked ~= nil then
				T.answer(T.strings.echo[3] .. printName(target, filename) .. T.strings.echo[4])
				return -1
			end

			T.answer(T.strings.echo[2] .. printName(target, filename) .. "...")
			T.answer( T.fileList[T.filePath_full(filename, target)].text[1])
			return (T.fileList[T.filePath_full(filename, target)].text[1])

		else
			table.remove(args,1)
			local temp = table.concat(args," ")
			T.answer(temp)
			return temp

		end
	end
end

T.encode = function(args)

	local setArgs = Set(args)

	if string.lower(args[2]) == "help" then
		T.answer(T.strings.encode[1])
		return args[1]

	else
		if table.getn(args) >= 3 then
			--encode phrase
			T.answer(T.strings.encode[2])
			local key = args[2]
			table.remove(args,1); table.remove(args,1);
			local out = coding.encrypt(table.concat(args, " "), key)
			T.answer(T.strings.encode[4] .. out)
			return out

		else
			--bad number of args
			T.answer(T.strings.encode[3])
			return -1

		end
	end
end

T.chtype = function(args)

	local setArgs = Set(args)

	if string.lower(args[2]) == "help" or table.getn(args) <= 1 then
		T.answer("chtype: Change type of the file.")
		T.answer("chtype: Sometimes it may matter, sometimes it may not.")
		T.answer("chtype: SYNTAX chtype [file] [type].")

		return args[1]

	else
		if table.getn(args) <= 2 then
			args[3] = "text"

		end
		local path = {args[2]}

		path = pathToGlobal(path)
		local filename = table.remove(path)

		if T.isFile_full(filename, path) and not isRootLocked(target[1]) then
			--file exists, read
			if T.fileList[T.filePath_full(filename, path)].script ~= nil then
				scripts[T.fileList[T.filePath_full(filename, path)].script[1]]("chtype");
			end

			if T.fileList[T.filePath_full(filename, path)].locked ~= nil then
				T.answer("chtype: ERROR File " .. printName(path, filename) .. " is locked!")
				return -1
			end

			local temp = T.fileList[T.filePath_full(filename, path)].type[1]
			T.fileList[T.filePath_full(filename, path)].type[1] = args[3]

			T.answer("chtype: File " .. printName(path, filename) .. " type changed from " .. temp .. " to " .. args[3] .. ".")

			return temp

		else
			T.answer("chtype: File " .. printName(path, filename) .. " does not exist!")
			return -1

		end

	end
end

T.decode = function(args)

	local setArgs = Set(args)

	if string.lower(args[2]) == "help" then
		T.answer(T.strings.decode[1])
		return args[1]

	else
		if table.getn(args) >= 3 then
			--decode
			T.answer(T.strings.decode[2])
			local key = args[2]
			table.remove(args,1); table.remove(args,1);
			local out = coding.decrypt(table.concat(args, " "), key)

			T.answer(T.strings.decode[4] .. out)
			return out

		else
			--bad number of args
			T.answer(T.strings.decode[3])
			return args[2]
		end

	end
end

T.cls = function(args)

	local setArgs = Set(args)

	if setArgs["help"] then
		T.answer(T.strings.cls[1])
		return args[1]

	else
		T.chatText = ""

	end
end

T.rpc = function(args)

	local setArgs = Set(args)

	if table.getn(args) <= 1 or string.lower(args[2]) == "help" then
		T.answer("rpc: Replaces phraseA in a phrase with phraseB.")
		T.answer("rpc: Place all phrases in quotation marks.")
		T.answer("rpc: To put a quotation mark inside a phrase, double it (\"\").")
		T.answer("rpc: If no quotation marks are found, or are unbalanced, first two words of a phrase are used as phraseA and phraseB.")
		T.answer("rpc: SYNTAX rpc \"[phraseA]\" \"[phraseB]\" \"[phrase]\"")
		return args[1]

	else
		if table.getn(args) <= 3 then
			T.answer("rpc: ERROR Bad number of arguments.")
			return -1

		else
			--check num of quot marks
			--if less than 6 do same as args 4, but first remove them
			local phr = {}
			table.remove(args,1);
			local str = table.concat(args," ");

			for k = 1, 3 do
				local st, en
				for i = 1, #str do
					if string.char(str:byte(i)) == "\"" then
						if st == nil then
							if i == #str then break end
							if i == 1 or ( string.char(str:byte(i+1)) ~= "\"" and string.char(str:byte(i-1)) ~= "\"" ) then
								st = i+1;
							end
						elseif en == nil then
							if i == #str or ( string.char(str:byte(i+1)) ~= "\"" and string.char(str:byte(i-1)) ~= "\"" ) then
								en = i-1;
							end
						end
					end
					if st ~= nil and en ~= nil then break end
				end
				if st ~= nil and en ~= nil then
					phr[k] = string.sub(str,st,en);
					if en ~= #str then
						str = string.sub(str,en+2,#str)

					end
				end
				if phr[k] == nil then break end
			end

			if phr[1]==nil then
				phr[1] = args[1];
			end

			if phr[2]==nil then
				phr[2] = args[2];
			end

			if phr[3]==nil then
				for i in string.gmatch(phr[1], "%S+") do
					table.remove(args,1);
				end
				for i in string.gmatch(phr[2], "%S+") do
					table.remove(args,1);
				end
				phr[3] = table.concat(args, " ");
			end

			--sub "" into ", but remember about """"
			--works better than intended, subbing any number of " into one
			--bug or feature?
			local indap = string.find(phr[3], "\"\"");
			while indap ~= nil do
				phr[3] = string.sub(phr[3], 1, indap) .. string.sub(phr[3], indap+2)
				indap = string.find(phr[3], "\"\"");
			end

			local tabphr = {}
			local num = 1
			for i in string.gmatch(phr[3], "%S+") do
				tabphr[num] = i
				num = num + 1
				if num > 2000 then break end
			end

			for i = 1, table.getn(tabphr) do
				if tabphr[i] == phr[1] then
					tabphr[i] = phr[2]
				end

			end
			str = table.concat(tabphr," ");

			T.answer("rpc: Phrase changed.")
			T.answer("rpc: New phrase:")
			T.answer("rpc: " .. str)
			return str;

		end
	end
end


T.play = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) == 1 then
		T.answer("play: Plays sound files.")
		T.answer("play: SYNTAX play [file]")
		return args[1]

	else

		local target = shallowcopy(args)
		table.remove(target,1)

		target = pathToGlobal(target)
		local filename = table.remove(target)

		if T.isFile_full(filename, target) and not isRootLocked(target[1]) then
			--if T.snd == nil then
			--file exists, read

			if T.fileList[T.filePath_full(filename, target)].script ~= nil then
				scripts[T.fileList[T.filePath_full(filename, target)].script[1]]("play");
			end

			if T.fileList[T.filePath_full(filename, target)].locked ~= nil then
				T.answer("play: ERROR File " .. printName(target, filename) .. " is locked.")
				return -1
			end

			T.answer("play: Playing " .. printName(target, filename) .. "...")
			local snd = audio.loadSound( "data/assets/audio/" .. T.fileList[T.filePath_full(filename, target)].audio[1] )
			table.insert(T.snd, audio.play( snd, {onComplete = function() audio.dispose( snd ) snd = nil end} ))

			return printName(target, filename)

		else
			T.answer("play: ERROR File " .. printName(target, filename) .. " does not exist.")
			return -1

		end
	end
end

T.stop = function(args)

	local setArgs = Set(args)

	if setArgs["help"] then
		T.answer("stop: Stops all playback and closes images.")
		T.answer("stop: SYNTAX stop")
		T.answer("stop: SYNTAX stop sound")
		T.answer("stop: SYNTAX stop image")
		return args[1]
	else
		local num = 0
		if setArgs["sound"] or table.getn(args) == 1 then
			if T.snd[1] ~= nil then
				for i = 2,32 do
					audio.stop(i)
				end
				for index,value in ipairs(T.snd) do
					T.snd[index] = nil
					num = num + 1
				end
				T.answer("stop: Sound(s) stopped.")
			end
		end

		if setArgs["image"] or table.getn(args) == 1 then
			if table.getn(T.imgObj) > 0 then
				for index,value in ipairs(T.imgObj) do
					value:dispatchEvent( {name="tap", target=value} )
					num = num + 1
				end
				T.answer("stop: Image(s) closed.")
			end
		end
		if num == 0 then
			T.answer("stop: Nothing was opened.")
		end

		return num
	end
end

T.prog = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) == 1 then
		T.answer("prog: Runs program file.")
		T.answer("prog: WARNING Do not run files of unknown origin.")
		T.answer("prog: SYNTAX prog [file]")
		return args[1]

	else

		local target = shallowcopy(args)
		table.remove(target,1)

		target = pathToGlobal(target)
		local filename = table.remove(target)

		if T.isFile_full(filename, target) and not isRootLocked(target[1]) then
			--file exists, read

			if T.fileList[T.filePath_full(filename, target)].script ~= nil then
				scripts[T.fileList[T.filePath_full(filename, target)].script[1]]("prog");
			end

			if T.fileList[T.filePath_full(filename, target)].locked ~= nil then
				T.answer("prog: ERROR File " .. printName(target, filename) .. " is locked.")
				return -1
			end

			T.answer("prog: Running " .. printName(target, filename) .. "...")
			if T.fileList[T.filePath_full(filename, target)].type[1] == "prog" then
				return T.run(T.fileList[T.filePath_full(filename, target)].prog[1])

			elseif T.fileList[T.filePath_full(filename, target)].type[1] == "app" then
				--run custom apps
				--dunno if necessary
				--may be cool to do that, all that is needed is a transition to other scene.lua
				--may be done with composer added to commands
				--no big deal
				--but functionality may be unused, if there will be no time to develop properly
				--*********************************
				--obligatory explanation
				--apps are located in data/assets/apps
				--app field in file must contain name corresponding to file in that directory
				--data/assets/apps/[NAME].lua
				--it may but not must contain custom data passed with file
				--(dunno if necessary, might be with the same app but different files)
				--always passes scripts
				--might be extended to pass other data, but commands are always available in the scene


				local options =
					{
						effect = "fade",
						time = 100,
						params = {
							--must pass scripts
							scripts = scripts,
						}
					}
				if T.fileList[T.filePath_full(filename, target)].custom ~= nil then
					--some params it may use?
					option.params.custom = T.fileList[T.filePath_full(filename, target)].custom

				end
				T.composer.gotoScene( "data.assets.apps." .. T.fileList[T.filePath_full(filename, target)].app[1], options )
				return printName(target, filename)

			else
				return T.run(T.fileList[T.filePath_full(filename, target)].text[1])

			end

		else
			T.answer("prog: File " .. printName(target, filename) .. " does not exist.")
			table.remove(args,1)
			local temp = table.concat(args," ")
			T.answer("prog: " .. temp)
			return T.run(temp)
		end
	end
end

T.show = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) == 1 then
		T.answer("show: Displays image files.")
		T.answer("show: SYNTAX show [file]")
		return args[1]

	else

		local target = shallowcopy(args)
		table.remove(target,1)

		target = pathToGlobal(target)
		local filename = table.remove(target)

		if T.isFile_full(filename, target) and not isRootLocked(target[1]) then
			--file exists, read

			if T.fileList[T.filePath_full(filename, target)].script ~= nil then
				scripts[T.fileList[T.filePath_full(filename, target)].script[1]]("show");
			end

			if T.fileList[T.filePath_full(filename, target)].locked ~= nil then
				T.answer("show: ERROR File " .. printName(target, filename) .. " is locked.")
				return -1
			end

			T.answer("show: Displaying " .. printName(target, filename) .. "...")
			local imgObj = display.newImageRect(T.group,
				"data/assets/images/" .. T.fileList[T.filePath_full(filename, target)].img[1],
				T.fileList[T.filePath_full(filename, target)].img[2],
				T.fileList[T.filePath_full(filename, target)].img[3])
			imgObj.x = 400
			imgObj.y = 640
			imgObj:addEventListener( "tap", function( event )
				for i, v in ipairs(T.imgObj) do
					if v == event.target then
						table.remove(T.imgObj,i)
						break
					end
				end
				event.target:removeSelf()
				event.target = nil
				return true

			end)

			imgObj:addEventListener( "touch", function( event )
				if ( event.phase == "began" ) then

					display.getCurrentStage():setFocus( event.target )
					event.target:toFront()
					event.target.isFocus = true

				elseif ( event.target.isFocus ) then
					if ( event.phase == "moved" ) then
						event.target.x = event.x
						event.target.y = event.y

					elseif ( event.phase == "ended" or event.phase == "cancelled" ) then
						display.getCurrentStage():setFocus( nil )
						event.target.isFocus = nil

					end
				end

				return true

			end)
			table.insert(T.imgObj,imgObj)

			return printName(target, filename)

		else
			T.answer("show: ERROR File " .. printName(target, filename) .. " does not exist.")
			return -1
		end
	end
end

T.plus = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) == 1 then
		T.answer("plus: Adds variables.")
		T.answer("plus: SYNTAX plus [var1] [var2]...")
		return args[1]

	else
		local plus = 0
		for i=2,table.getn(args) do
			if tonumber(args[i]) ~= nil then
				plus = plus + tonumber(args[i])
			end
		end
		plus = tostring(plus)
		T.answer("plus: " .. plus)
		return plus

	end
end

T.minus = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) == 1 then
		T.answer("minus: Subtracts variables.")
		T.answer("minus: SYNTAX minus [var1] [var2]...")
		return args[1]

	else
		local minus = 0
		for i=2,table.getn(args) do
			if tonumber(args[i]) ~= nil then
				minus = minus - tonumber(args[i])
			end
		end
		minus = tostring(minus)
		T.answer("minus: " .. minus)
		return minus

	end
end

T.multip = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) == 1 then
		T.answer("multip: Multiplies variables.")
		T.answer("multip: SYNTAX multip [var1] [var2]...")
		return args[1]

	else
		local multip = 1
		for i=2,table.getn(args) do
			if tonumber(args[i]) ~= nil then
				multip = multip * tonumber(args[i])
			end

		end
		multip = tostring(multip)
		T.answer("multip: " .. multip)
		return multip

	end
end

T.divide = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) <= 2 then
		T.answer("divide: Divides two variables.")
		T.answer("divide: SYNTAX divide [var1] [var2]")
		return args[1]

	else
		if tonumber(args[2]) ~= nil and tonumber(args[3]) ~= nil then
			local div = tostring(args[2] / args[3])

			T.answer("divide: " .. div)
			return div

		else
			T.answer("divide: ERROR cannot divide " .. args[2] .. " by " .. args[3])
			return -1

		end
	end
end

T.rt = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) <= 2 then
		T.answer("rt: Returns root of the N degree of var.")
		T.answer("rt: SYNTAX rt [var] [N]")
		return args[1]

	else
		if tonumber(args[2]) ~= nil and tonumber(args[3]) ~= nil then
			local rt = math.pow(tonumber(args[2]),1/tonumber(args[3]))

			T.answer("rt: " .. rt)
			return rt

		else
			T.answer("rt: ERROR cannot calculate root of " .. args[2] .. " of degree " .. args[3])
			return -1

		end
	end
end

T.readvar = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) <= 1 then
		T.answer("readvar: Returns value of variable.")
		T.answer("readvar: SYNTAX readvar [var]")
		return args[1]

	else
		if T.varList[args[2]] ~= nil then
			T.answer("readvar: " .. T.varList[args[2]])
			return T.varList[args[2]]

		else
			T.answer("readvar: Variable " .. args[2] .. " not found.")
			return -1

		end
	end
end

T.remvar = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) <= 1 then
		T.answer("remvar: Removes variable from memory.")
		T.answer("remvar: WARNING Please, be careful.")
		T.answer("remvar: SYNTAX remvar [var]")
		return args[1]

	else
		if T.varList[args[2]] ~= nil then
			T.answer("remvar: Variable " .. T.varList[args[2]] .. " removed.")
			local temp = T.varList[args[2]]
			T.varList[args[2]] = nil
			return temp

		else
			T.answer("remvar: Variable " .. args[2] .. " not found.")
			return -1

		end
	end
end

T.remfile = function(args)
	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) <= 1 then
		T.answer("remfile: Removes a file.")
		T.answer("remfile: WARNING PLEASE BE CAREFUL.")
		T.answer("remfile: SYNTAX remfile [file]")
		return args[1]

	else
		local target = shallowcopy(args)
		table.remove(target,1)

		target = pathToGlobal(target)
		local filename = table.remove(target)

		if T.isFile_full(filename, target) and not isRootLocked(target[1]) then
			local curr = T.datStuct
			for index,value in ipairs(target) do curr = curr[value] end

			T.fileList[T.filePath_full(filename, target)] = nil

			for i,v in ipairs(curr.files) do
				if v == filename then
					table.remove(curr.files,i)
					break;
				end
			end


			T.answer("remfile: File " .. printName(target, filename) .. " removed.")
			return printName(target, filename)

		else
			T.answer("remfile: ERROR File " .. printName(target, filename) .. " doesn't exist.")
			return -1

		end

	end

end

T.remdir = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) <= 1 then
		T.answer("remdir: Removes directory and all it's contents.")
		T.answer("remdir: WARNING PLEASE BE CAREFUL.")
		T.answer("remdir: SYNTAX remdir [dir]")
		return args[1]

	else
		--if dir exists, remove it

		local path = {args[2]}

		path = pathToGlobal(path)
		local target = table.remove(path)

		if path[1] == nil then
			T.answer("remdir: ERROR Bad path specified.")
			T.answer("remdir: WARNING PLEASE PLEASE PLEASE DON'T KILL US ALL.")
			return -1

		end

		if isRootLocked(path[1]) then
			T.answer("remdir: ERROR No access to the directory.")
			T.answer("remdir: WARNING WARNING WARNING WARNING")
			T.answer("remdir: PLEASE BE SURE WHAT YOU'RE DOING")
			T.answer("remdir: WARNING WARNING WARNING WARNING")
			return -1

		end

		local curr = T.datStuct
		for index,value in ipairs(path) do curr = curr[value] end
		if type(curr[tostring(target)]) == "table" and curr[tostring(target)] ~= "files" then
			if args[3] == nil then args[3] = 'n' end
			if isEmpty_full(path, target) or string.lower(args[3])=="y" then
				curr[target] = nil
				--table.remove(T.activeDir, tostring(args[2]))
				local temp = T.filePath_full(target, path)
				for k,v in pairs(T.fileList) do
					local s, n = string.find(k,temp)
					--if string is found
					if s ~= nil then
						--if string begins with the path
						if s == 1 then
							--if string does not end with the path
							if n < #k then
								T.fileList[k] = nil
							end
						end
					end
				end
				T.answer("remdir: Directory " .. printName(path, target) .. " removed.")
				T.answer("remdir: WARNING HOPEFULLY EVERYTHING WORKS STILL.")
				return printName(path, target)

			else
				T.answer("remdir: Directory " .. printName(path, target) .. " not empty.")
				T.answer("remdir: Use \"remdir [dir] y\" to force remove.")
				T.answer("remdir: WARNING BUT FIRST MAKE SURE YOU REALLY WANT TO.")
				return -1

			end

		else
			T.answer("remdir: ERROR No such directory.")
			T.answer("remdir: WARNING PLEASE DON'T TRY AGAIN.")
			return -1

		end

	end
end


T.mail = function(args)
	local setArgs = Set(args)

	if args[2] == "help" or table.getn(args) <= 1 then
		T.answer("mail: Reads mail archive.")
		T.answer("mail: SYNTAX mail [file]")
		return args[1]

	else

		local target = shallowcopy(args)
		table.remove(target,1)

		target = pathToGlobal(target)
		local filename = table.remove(target)

		if T.isFile_full(filename, target) and not isRootLocked(target[1]) then
			--is istype mail

			--script check and locked check

			if T.fileList[T.filePath_full(filename, target)].script ~= nil then
				scripts[T.fileList[T.filePath_full(filename, target)].script[1]]("mail");
			end

			if T.fileList[T.filePath_full(filename, target)].locked ~= nil then
				T.answer("mail: ERROR File " .. printName(target, filename) .. " is LOCKED! PICNIC!")
				return -1
			end

			if T.fileList[T.filePath_full(filename, target)].type[1] == "mail" then
				local file = table.concat(target,"") .. filename

				if T.fileList[T.filePath_full(filename, target)].mail ~= nil then
					file = T.fileList[T.filePath_full(filename, target)].mail[1]

				end

				local options =
					{
						effect = "fade",
						time = 100,
						params = {
							file = file,
							scripts = scripts,
						}
					}
				T.composer.gotoScene( "data.assets.apps.mail-read", options )
				return printName(target, filename)

			else
				T.answer("mail: ERROR File " .. printName(target, filename) .. " is not a mail archive.")
				return -1

			end

		else
			T.answer("mail: ERROR File " .. printName(target, filename) .. " doesn't exist.")
			return -1

		end

	end

end

T.root = function(args)

	local setArgs = Set(args)

	if setArgs["help"] then
		T.answer("root: Returns to or changes root directory.")
		T.answer("root: SYNTAX root")
		T.answer("root: SYNTAX root list")
		T.answer("root: SYNTAX root [rootdir]")
		T.answer("root: SYNTAX root [rootdir] [pass]")
		return args[1]

	elseif table.getn(args)<=1 then
		T.activeDir = {T.activeDir[1]}
		T.answer("root: Returned to root " .. T.activeDir[1] .. ".")
		return T.activeDir[1]

	else
		if setArgs["list"] then
			--display list of rootCats, also with locked/unlocked
			T.answer("root: Listing root directories:")
			for k,v in pairs(T.dirList) do
				local msg = tostring(k)
				if v.locked == 1 then
					msg = msg .. "   \tlocked"
				else
					msg = msg .. "   \tunlocked"
				end
				T.answer(msg)
			end
			return T.activeDir[1]

		else
			args[2] = string.upper(args[2])
			if isRoot(args[2]) then--check if rootcat exists Set(T.dirList)[args[2]] or Set(T.hiddDirList)[args[2]]

				if not isRootLocked(args[2]) then
					T.activeDir = nil
					T.activeDir = {args[2]}
					T.answer("root: Current rootdir set to " .. args[2])
					return args[2]

			else
				if args[3] ~= nil and args[3] == T.dirList[args[2]].pass then
					--unlock and goto
					T.dirList[args[2]].locked = 0 --unlock
					T.answer("root: Rootdir " .. args[2] .. " unlocked.")
					return T.root({"rooot", args[2]}) --chdir

				elseif args[3] ~= nil then

					T.answer("root: Wrong password to " .. args[2] .. ".")
					return -1
				else

					T.answer("root: Rootdir " .. args[2] .. " locked.")
					return -1
				end
			end
			else
				--rootcat does not exist
				T.answer("root: Rootdir " .. args[2] .. " does not exist.")
				return -1

			end
		end
	end
end

T.quit = function(args)

	local setArgs = Set(args)

	if setArgs["help"] then
		T.answer("quit: Shuts down program.")
		return args[1]

	else
		T.answer("quit: Goodbye.")
		native.setKeyboardFocus(nil)
		timer.performWithDelay(600, function()
			T.stop({"stop"})
			T.cls({"cls"})
			T.incent();
			T.globalSave()
		end)
		timer.performWithDelay(800, function()
			--if "Android" == system.getInfo( "platformName" ) then
			native.requestExit()
			--end
		end)
		return 1
	end
end

T.len = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) <= 1 then
		T.answer("len: Returns number of words in [phrase].")
		T.answer("len: Maybe useful some day.")
		T.answer("len: SYNTAX len [phrase]")
		return args[1]

	else
		T.answer("len: " .. tostring(table.getn(args)-1))
		return tostring(table.getn(args)-1)

	end
end


T._or = function(args)
	if table.getn(args) <= 1 or args[2]=="help" then
		T.answer("or: Logical \"or\" operation.")
		T.answer("or: Returns \"y\" if at least one argument is not equal to \"n\" or \"-1\".")
		T.answer("or: SYNTAX or [var1] [var2] ...")
		return "or"

	else
		for i = 2, table.getn(args) do
			if args[i] ~= -1 and args[i] ~= 'n' then
				T.answer("or: y")
				return 'y'

			end
		end
		T.answer("or: n")
		return 'n'
	end

end

T._and = function(args)
	if table.getn(args) <= 1 or args[2]=="help" then
		T.answer("and: Logical \"and\" operation.")
		T.answer("and: Returns \"y\" if all arguments are not equal to \"n\" or \"-1\".")
		T.answer("and: SYNTAX and [var1] [var2] ...")
		return "or"

	else
		for i = 2, table.getn(args) do
			if args[i] == -1 or args[i] == 'n' then
				T.answer("and: n")
				return 'n'

			end
		end
		T.answer("and: y")
		return 'y'
	end
end

T.no = function(args)

	local setArgs = Set(args)

	if args[2]=="help" then
		T.answer("no: there is truly no help :<")
		return 'n'

	else
		local anwser = " "
		if table.getn(args) == 1 then
			anwser = anwser .. 'n '
		else
			for i = 2, table.getn(args) do
				if args[i] == -1 or args[i] == 'n' then
					anwser = anwser .. 'y '

				else
					anwser = anwser .. 'n '

				end
			end
		end
		T.answer("no: " .. anwser)
		return anwser

	end
end

T.toPath = function(args)

	local setArgs = Set(args)

	if args[2]=="help" or table.getn(args) < 2 then
		T.answer("topath: Changes arguments into full path.")
		T.answer("topath: Some commands may require single-argument path.")
		T.answer("topath: SYNTAX topath [arg1] [arg2] ...")
		return args[1]

	else
		local path = shallowcopy(args)
		table.remove(path,1)

		path = pathToGlobal(path)
		path = table.concat(path,"::")

		T.answer("topath: Your full path estimated to be: " .. path)
		return path

	end
end


T.softreset = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) <= 1 then
		T.answer("softreset: Restores file system in current root directory.")
		T.answer("softreset: Also resets variables in memory.")
		T.answer("softreset: Useful, if something went wrong.")
		T.answer("softreset: SYNTAX softreset y")
		return args[1]

	else
		for i = 2,table.getn(args) do
			if args[i] ~= "y" then
				T.answer("softreset: Unrecognized parameter.")
				T.answer("softreset: I'm so sorry:<")
				return -1

			end
		end
		if table.getn(args) == 2 then
			T.answer("softreset: WARNING Please be sure you know what you're doing.")
			T.answer("softreset: This operation will wipe all changes in current root directory.")
			T.answer("softreset: Also, reset all variables saved in the environment to factory default.")
			T.answer("softreset: SYNTAX softreset y y")
			return -1

		else
			T.root({"root"})

			local path = "data/origData/variables.json"
			path = system.pathForFile(path, system.ResourceDirectory);
			local file = io.open(path, "r")
			local jsonFile = file:read("*a")
			local t = json.decode( jsonFile )
			io.close(file)
			T.varList = t

			path = "data/origData/struct.json"
			path = system.pathForFile(path, system.ResourceDirectory);
			file = io.open(path, "r")
			jsonFile = file:read("*a")
			t = json.decode( jsonFile )
			io.close(file)
			T.datStuct[T.activeDir[1]] = t[T.activeDir[1]]

			path = "data/origData/files.json"
			path = system.pathForFile(path, system.ResourceDirectory);
			file = io.open(path, "r")
			jsonFile = file:read("*a")
			t = json.decode( jsonFile )
			io.close(file)

			local temp = table.concat(T.activeDir,"")
			for k,v in pairs(t) do
				if string.find(k,temp) ~= nil then
					T.fileList[k] = t[k]
				end
			end

			jsonFile = nil
			file = nil

			T.stop({"stop"})
			T.cls({"cls"})
			T.answer("softreset: Soft reset done succesfully:)")
			return 0

		end

	end
end

T.bcgcolor = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) <= 1 then
		T.answer("bcgcolor: Sets default background color.")
		T.answer("bcgcolor: SYNTAX bcgcolor [color].")
		T.answer("bcgcolor: SYNTAX bcgcolor [num1] [num2] [num3].")
		T.answer("bcgcolor: nums < 256.")
		T.answer("bcgcolor: TODO merge this with \"fontcolor\" as \"color\"")
		return table.concat(T.bcgColor," ")

	else
		if table.getn(args) == 2 then
			if setArgs["gold"] then
				T.bcgColor = {247/255, 188/255, 30/255}

			elseif setArgs["blue"] then
				T.bcgColor = {68/255, 171/255, 215/255}

			elseif setArgs["red"] then
				T.bcgColor = {229/255, 50/255, 21/255}

			elseif setArgs["green"] then
				T.bcgColor = {22/255, 233/255, 45/255}

			elseif setArgs["grey"] then
				T.bcgColor = {0.5, 0.5, 0.5}

			elseif setArgs["black"] then
				T.bcgColor = {0, 0, 0}

			else
				T.answer("bcgcolor: Unrecognized color word " .. args[2])
				return -1

			end

		elseif table.getn(args) > 3 then
			for i = 2, 4 do
				if tonumber(args[i]) ~= nil then
					if tonumber(args[i]) > 255 then
						args[i] = 1
					elseif tonumber(args[i]) < 0 then
						args[i] = 0
					else
						args[i] = args[i]/255
					end
				else
					T.answer("bcgcolor: Bad argument " .. args[i])
					return -1
				end
			end
			T.bcgColor = {args[2], args[3], args[4]}
		end
	end
	T.answer("bcgcolor: Enjoy your new color!")
	return table.concat(T.bcgColor," ")
end

T.fontcolor = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) <= 1 then
		T.answer("fontcolor: Sets default font color.")
		T.answer("fontcolor: SYNTAX fontcolor [color].")
		T.answer("fontcolor: SYNTAX fontcolor [num1] [num2] [num3].")
		T.answer("fontcolor: nums < 256.")
		return table.concat(T.defColor," ")

	else
		if table.getn(args) == 2 then
			if setArgs["gold"] then
				T.defColor = {247/255, 188/255, 30/255}

			elseif setArgs["blue"] then
				T.defColor = {68/255, 171/255, 215/255}

			elseif setArgs["red"] then
				T.defColor = {229/255, 50/255, 21/255}

			elseif setArgs["green"] then
				T.defColor = {22/255, 233/255, 45/255}

			elseif setArgs["grey"] then
				T.defColor = {0.5, 0.5, 0.5}

			elseif setArgs["black"] then
				T.defColor = {0, 0, 0}

			else
				T.answer("fontcolor: Unrecognized color word " .. args[2])
				return -1

			end

		elseif table.getn(args) > 3 then
			for i = 2, 4 do
				if tonumber(args[i]) ~= nil then
					if tonumber(args[i]) > 255 then
						args[i] = 1
					elseif tonumber(args[i]) < 0 then
						args[i] = 0
					else
						args[i] = args[i]/255
					end
				else
					T.answer("fontcolor: Bad argument " .. args[i])
					return -1
				end
			end
			T.defColor = {args[2], args[3], args[4]}
		end
	end
	T.answer("fontcolor: Enjoy your new color!")
	return table.concat(T.defColor," ")
end

T.setvar = function(args)

	local setArgs = Set(args)

	if args[2] == "help" or table.getn(args) <= 2 then
		T.answer("setvar: Sets variable [var] to [phrase]")
		T.answer("setvar: SYNTAX setvar [var] [phrase]")
		return args[1]

	elseif T.listSet[args[2]] or T.hiddListSet[args[2]] or string.lower(args[2])=="n" or
		string.lower(args[2])=="y" or tonumber(args[2])~=nil  then
		T.answer("setvar: ERROR Variable name restricted.")
		T.answer("setvar: For protection start names with \"var_\".")
		return -1

	elseif T.varList[args[2]] ~= nil and string.lower(args[3])~="y" then
		T.answer("setvar: ERROR Variable already exists.")
		T.answer("setvar: To override use following:")
		T.answer("setvar: SYNTAX setvar [var] y [phrase]")
		return -1

	elseif T.varList[args[2]] ~= nil and string.lower(args[3])=="y" then
		table.remove(args, 1)
		local temp = table.remove(args, 1)
		table.remove(args, 1)
		local phrase = table.concat(args," ")
		local oldphrase = T.varList[temp]
		T.varList[temp] = phrase
		T.answer("setvar: Variable " .. temp .. " set to " .. phrase .. " from " .. oldphrase)
		return phrase

	elseif T.varList[args[2]] == nil then
		table.remove(args, 1)
		local temp = table.remove(args, 1)
		local phrase = table.concat(args," ")
		T.varList[temp] = phrase
		T.answer("setvar: Variable " .. temp .. " set to " .. phrase )
		return phrase

	end
end

T.hiddensetvar = function(args)

	table.remove(args, 1)
	local temp = table.remove(args, 1)
	local phrase = table.concat(args," ")
	T.varList[temp] = phrase

end

T.equals = function(args)

	local setArgs = Set(args)

	if setArgs["help"] or table.getn(args) <= 2 then
		T.answer("equals: Returns \"y\" if all vars are equal, ore \"n\" if not.")
		T.answer("equals: SYNTAX equals [var1] [var2]...")
		return args[1]

	else
		for i=2,table.getn(args)-1 do
			if args[i] ~= args[i+1] then
				T.answer("equals: n")
				return "n"
			end
		end

		T.answer("equals: y")
		return "y"
	end
end

T.note = function(args)

	if table.getn(args) == 1 then
		local options =
			{
				effect = "fade",
				time = 100,
				params = {
					scripts = scripts
				}
			}
		T.composer.gotoScene( "data.assets.apps.notepad-app", options )
		return args[1]

	elseif string.lower(args[2]) == "help" then
		T.answer("note: Opens DATNote or writes a note.")
		T.answer("note: Use DATNote to view or remove notes.")
		T.answer("note: Use \"m\" parameter to write a note.")
		T.answer("note: SYNTAX note")
		T.answer("note: SYNTAX note m [message]")
		return args[1]

	elseif string.lower(args[2]) == "m" then
		table.remove(args,1); table.remove(args,1); --remove "note" and "m" from args
		table.insert(T.notes,{os.date( "%c", os.time() + 245489458 ), table.concat(args, " ")})
		T.answer("note: Note added!")
		return table.concat(args, " ")

	else
		T.answer("note: ERROR Unknown argument.\nnote: Try \"note help\" for more information.")
		return -1

	end

end

T.cond = function(args)
	--cond then else instruction, bcs "if" is restricted
	local setArgs = Set(args)

	if table.getn(args) == 1 then
		T.answer("cond: Conditional instruction.")
		T.answer("cond: If condition equals to \"0\" or \"n\", [expression2] will happen, otherwise [expression] is executed.")
		T.answer("cond: SYNTAX cond [condition] then [expression]")
		T.answer("cond: SYNTAX cond [condition] then [expression] else [expression2]")
		return args[1]

	else

		--search for last "then" argument
		local lastthen = nil
		for index,value in ipairs(args) do
			if value == "then" then lastthen = index end
		end

		--in case then is not found
		if lastthen == nil then
			T.answer("cond: ERROR Bad syntax.")
			return -1

		end

		--find else
		local lastelse = nil
		for index,value in ipairs(args) do
			if value == "else" then lastelse = index end
		end

		--create values to return
		local returnargs = ""
		--remove "cond"
		table.remove(args, 1)
		--remove condition
		local condition = table.remove(args, 1)
		--remove "then"
		table.remove(args, 1)

		local var = T.run(condition)

		if lastelse == nil then

			if var == "n" or var == -1 then
				returnargs = "n";

			else
				returnargs = T.run(table.concat(args," "))

			end

		else
			--after "then" there is expression
			local truargs = table.remove(args, 1)
			--remove "else"
			table.remove(args, 1)

			if var == "n" or var == -1 then
				returnargs = T.run(table.concat(args," "))

			else
				returnargs = T.run(truargs)

			end

		end

		return returnargs
	end



end

T.loop = function(args)
	--loop over, instead of for do, bcs reasons
	local setArgs = Set(args)

	if table.getn(args) == 1 then
		T.answer("loop: Used to loop a loop.")
		T.answer("loop: Use \"_iter\" inside expression to access iterator.")
		T.answer("loop: SYNTAX loop [expression] over [iterator]")
		T.answer("loop: PSA Bad looping may cause unpredicted errors.")
		T.answer("loop: PSA Stop breaking everything you dumdum.")
		return args[1]

	end

	--search for last "over" argument
	local lastover = nil
	for index,value in ipairs(args) do
		if value == "over" then lastover = index end
	end

	--in cas over is not found
	if lastover == nil then
		T.answer("loop: ERROR Bad syntax.")
		T.answer("loop: If it's too much for you, don't sweat.")
		T.answer("loop: Just give up.")
		return -1

	end

	--create list to iterate over
	local iter = {}
	if lastover == table.getn(args)-1 then
		if tonumber(args[table.getn(args)]) ~= nil then
			for i = 1, tonumber(args[#args]) do
				iter[i] = i
				if i > 2000 then break end
			end
		else
			local num = 1
			for i in string.gmatch(args[#args], "%S+") do
				iter[num] = i
				num = num + 1
				if num > 2000 then break end
			end
		end
	else
		for i = 1,#args-lastover do
			table.insert(iter, 1, table.remove(args))
		end

	end

	--create variable to contain return values, also remove "loop" and "over"
	local returnargs = ""
	table.remove(args, 1)
	table.remove(args)

	--now run looped code for each item in iter list
	for i,v in ipairs(iter) do
		--change text code into arguments list
		local newargs = {}
		local num = 1
		for i in string.gmatch(args[1], "%S+") do
			newargs[num] = i
			num = num + 1
			if num > 2000 then break end
		end
		--change _iter to value
		for index,value in ipairs(newargs) do
			if value == "_iter" then
				--change _iter to value, but not in nested loops
				local iloops = 0
				local iovers = 0
				for j = 1, index do
					if newargs[j] == "loop" then
						iloops = iloops + 1
					end
					if newargs[j] == "over" then
						iovers = iovers + 1
					end
				end
				if iovers >= iloops then
					newargs[index] = v

				end

			end
		end
		--run looped code, and add output to return values
		if newargs[1] ~= nil then
			returnargs = returnargs .. " " .. T.run(table.concat(newargs," "))

		else
			T.answer("loop: ERROR Bad syntax.")
			T.answer("loop: Looping is really hard.")
			T.answer("loop: Better stop before you break something.")

		end

	end
	--return
	return returnargs

end

T.isFile = function(args)
	local setArgs = Set(args)

	if table.getn(args) == 1 then
		T.answer("isFile: Checks if argument is a file.")
		T.answer("isFile: SYNTAX isFile [file]")
		return args[1]

	else

		local target = shallowcopy(args)
		table.remove(target,1)

		target = pathToGlobal(target)
		local filename = table.remove(target)

		if T.isFile_full(filename, target) and not isRootLocked(target[1]) then
			T.answer("isFile: " .. printName(target, filename) ..  " is a file.")
			return "y"

		else
			T.answer("isFile: " .. printName(target, filename) ..  " is not a file.")
			return "n"

		end

	end

end

T.rand = function(args)
	local setArgs = Set(args)
	local r

	if setArgs["help"] then
		T.answer("rand: Returns random number.")
		T.answer("rand: SYNTAX rand")
		T.answer("rand: SYNTAX rand [max]")
		T.answer("rand: SYNTAX rand [min] [max]")
		return args[1]

	else
		if table.getn(args) == 1 then
			r = math.round(math.random())
			T.answer("rand: r = " .. r)
			return r

		elseif table.getn(args) == 2 then
			if tonumber(args[2]) == nil or tonumber(args[2]) < 1 then
				T.answer("rand: Bad argument!")
				return -1

			end
			r = math.random(tonumber(args[2]))
			T.answer("rand: r = " .. r)
			return r

		else
			if tonumber(args[2]) == nil or tonumber(args[3]) == nil
				or tonumber(args[2]) < 1 or tonumber(args[3]) < 1 or tonumber(args[3]) < tonumber(args[2]) then
				T.answer("rand: Bad argument!")
				return -1

			end
			r = math.random(tonumber(args[2]),tonumber(args[3]))
			T.answer("rand: r = " .. r)
			return r
		end
	end

end

T.unlock = function(args)
	local setArgs = Set(args)
	if setArgs["help"] or table.getn(args) == 1 then
		T.answer("unlock: Unlocks file.")
		T.answer("unlock: SYNTAX unlock [password] [filename]")
		return args[1]

	elseif table.getn(args) == 2 then
		T.answer("unlock: ERROR Not enough arguments!")
		return -1

	else
		local target = shallowcopy(args)
		table.remove(target,1)
		table.remove(target,1)

		target = pathToGlobal(target)
		local filename = table.remove(target)

		if T.isFile_full(filename, target) and not isRootLocked(target[1]) then
			if T.fileList[T.filePath_full(filename, target)].script ~= nil then
				scripts[T.fileList[T.filePath_full(filename, target)].script[1]]({"unlock", args[2]});
			end

			if T.fileList[T.filePath_full(filename, target)].locked ~= nil then
				if T.fileList[T.filePath_full(filename, target)].locked == args[2] then
					--TODO create unlocked file (DONE?)
					local newname = filename .. "_u"

					while T.isFile_full(newname, target) do
						newname = newname .. "_u"

					end

					local curr = T.datStuct
					for index,value in ipairs(target) do curr = curr[value] end
					table.insert(curr.files,newname)

					T.fileList[T.filePath_full(newname, target)] = {}

					for k, value in pairs(T.fileList[T.filePath_full(filename, target)]) do
						T.fileList[T.filePath_full(newname, target)][k] = {}

						for i, v in ipairs(value) do
							table.insert(T.fileList[T.filePath_full(newname, target)][k],i,v)

						end

					end
					T.fileList[T.filePath_full(newname, target)].locked = nil;
					T.answer("unlock: File " .. printName(target, newname) .. " created!")

					return 1

				else
					T.answer("unlock: ERROR Wrong password!")
					return -1

				end

			else

				T.answer("unlock: ERROR File " .. printName(target, filename) .. " already unlocked!")
				return -1
			end

		else
			T.answer("unlock: ERROR File " .. printName(target, filename) .. " not found!")
			return -1

		end

	end

end

return T;
